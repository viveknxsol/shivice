<?php
include("conn.inc");
session_start();
  
//Smarty Settings :Start
require('./include/smarty/libs/Smarty.class.php');  // put full path to Smarty.class.php
$smarty = new Smarty();

$smarty->setTemplateDir('./templates');
$smarty->setCompileDir ('./include/smarty/omTemplates_c');
$smarty->setCacheDir   ('./include/smarty/omCache');
$smarty->setConfigDir  ('./include/smarty/omConfigs');
//Smarty Settings :End
date_default_timezone_set ("Asia/Calcutta");
?>