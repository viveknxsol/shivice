<?php
include("include/config.inc.php");
if (!isset($_SESSION['s_activId'])) {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];    
    header("Location:checkLogin.php");
}
$orderId=isset($_REQUEST['orderId']) ? $_REQUEST['orderId'] : 0;
$orderArray = array();
$totalAmount=0;
$totalItems=0;
$i = 0;
$recordFound = 0;
$joinQueryResult = array();


$selectOrder    = "SELECT item.itemName,item.itemPrice, item.kgPrice, ordereditems.quantity,ordereditems.orderTime,ordereditems.parcel,ordereditems.ordereditemsId ,ordereditems.orderId, waiter.waiterName,
                            ordermaster.tableId,ordereditems.itemId,waiter.waiterId,ordermaster.tableId, ordermaster.tablePart, ordereditems.weight,ordereditems.userName
                       FROM ordereditems 
                  LEFT JOIN ordermaster ON ordereditems.orderId = ordermaster.orderId
                  LEFT JOIN item ON ordereditems.itemId =  item.itemId
                  LEFT JOIN waiter ON ordermaster.waiterId = waiter.waiterId
                      WHERE ordermaster.orderId = ".$orderId."
                   ORDER BY ordereditems.orderedItemsId DESC";
  $selectOrderRes = mysql_query($selectOrder);
  if(mysql_num_rows($selectOrderRes) > 0)
  {
    while($orderRow = mysql_fetch_array($selectOrderRes))
    {
      $recordFound = 1;
      $orderArray[$i]['weight']          = $orderRow['weight'];
      $orderArray[$i]['orderId']         = $orderRow['orderId'];
      $orderArray[$i]['waiterId']        = $orderRow['waiterId'];
      $orderArray[$i]['tableId']         = $orderRow['tableId'];
      $orderArray[$i]['tablePart']       = $orderRow['tablePart'];
      $orderArray[$i]['itemName']        = $orderRow['itemName'];      
      $orderArray[$i]['kgPrice']         = $orderRow['kgPrice'];
      $orderArray[$i]['quantity']        = $orderRow['quantity'];  $totalItems+=$orderArray[$i]['quantity'];
      $orderArray[$i]['userName']        = $orderRow['userName']; 
      
      if($orderArray[$i]['weight'] == 0)
      {
       $orderArray[$i]['amount']         = $orderRow['itemPrice'] * $orderRow['quantity'];
       $orderArray[$i]['itemPrice']       = $orderRow['itemPrice'];
      }
      else
      {
        $orderArray[$i]['amount']        = ceil((($orderRow['kgPrice'] * $orderRow['weight'])/1000)*$orderRow['quantity']);
        $orderArray[$i]['itemPrice']       = $orderRow['kgPrice'];        
      }
      $orderArray[$i]['waiterName']                        = $orderRow['waiterName'];
      $orderArray[$i]['orderTime']       = $orderRow['orderTime'];    
      $orderArray[$i]['itemId']          = $orderRow['itemId']."\n";    
      $orderArray[$i]['ordereditemsId']  = $orderRow['ordereditemsId'];
      $orderArray[$i]['parcel']          = $orderRow['parcel'];
      
      $totalAmount                       = $totalAmount + $orderArray[$i]['amount'];
      
      $i++;
    }	
  }
include("./bottom.php");
$smarty->assign('recordFound',$recordFound);
$smarty->assign('orderArray',$orderArray);
$smarty->assign('totalAmount',$totalAmount);
$smarty->assign('totalItems',$totalItems);
$smarty->assign('joinQueryResult', $joinQueryResult);
$smarty->display("viewTable.tpl");
?>