<table border="1" width="100%" id="carOrderTable" valign="top">
    {foreach from=$carOrders item=carItems name=orderForEach}
        {math equation="x % y" x=$smarty.foreach.orderForEach.iteration y=2 assign="iterationModulus"}
        {if $iterationModulus == 1}
            <tr valign="top">
            {/if}
            <td id="{$carItems.orderId}" ondblclick="printFun({$carItems.orderId});" align="center">
                <b><font color="red" size="5">{$carItems.waiterName} {$carItems.tableId1} </font></b>
                <table border="1" cellpadding="2" cellspacing="0" id="{$carItems.orderId}" valign="top"  onclick="generateCarOrder(this.id);">
                    <tr>
                        <th>Item</th>
                        <th>Qty.</th>
                        <th>Rs.</th>
                    </tr>
                    {foreach from=$carItems item=carItemsElement name=itemsElement}
                        <tr>                   
                            {if $smarty.foreach.itemsElement.iteration > 4}
                                {foreach from=$carItemsElement item=carItems}
                                    <td cellpadding="2">
                                        {$carItems}
                                    </td>
                                {/foreach}
                            {/if}
                        </tr>  
                    {/foreach}
                </table>
            </td>
            {if $iterationModulus == 0}
            </tr>
        {/if}
    {/foreach}
</table>
<script type="text/javascript">
    function generateCarOrder(a)
    {
        window.parent.location.href = "setUrlCar.php?orderId=" + a;
    }
    function printFun(orderIdVar)
    {
        var url = "amountListPdf1.php?orderId=" + orderIdVar;
        var win = window.open("amountListPdf1.php?orderId=" + orderIdVar);
        win.window.print();
        setTimeout(function () {
            win.close();
            setTimeout("window.parent.reload(true);", 500);
        }, 500);
    }

    //Page Refresh in 5 sec..
    //window.onload = setupRefresh;
    //function setupRefresh() {
    //setTimeout("refreshPage();", 5000);
    //}
    //function refreshPage() {
    //window.location = location.href;
    // }
</script>