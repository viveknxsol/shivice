<?php

include("include/config.inc.php");
$selectTableForCarRow = "0";
if (!isset($_SESSION['s_activId'])) {
    header("Location:checkLogin.php");
}

if ($_SESSION['s_userType'] == "admin") {
    $smarty->assign("userType", "admin");
    //echo "<script> alert('".$_SESSION['s_userType']."')</script>";
} else {
    $smarty->assign("userType", "local");
    //echo "<script> alert('".$_SESSION['s_userType']."')</script>";
}

if (!(isset($_REQUEST['waiter'])) AND ! (isset($_REQUEST['table'])) AND ! (isset($_REQUEST['tablePart']))) {
    $waiterVar = "1";
    $tableVar = "1";
    $tablePartVar = "A";
    $waiterName = "0";
    $carNo = "0";
} else {
    $waiterVar = $_REQUEST['waiter'];
    if (isset($_REQUEST['changeCar']) && $_REQUEST['changeCar'] == 1) {
        $selectTableForCar = "SELECT MAX(tableId) AS tableId 
                            FROM ordermaster 
                           WHERE tablePart = 'C'";
        $selectTableForCarRes = mysql_query($selectTableForCar);
        $selectTableForCarRow = mysql_fetch_array($selectTableForCarRes);

        $tableVar = $selectTableForCarRow['tableId'] + 1;
        if ($tableVar <= 36) {
            $tableVar = 37;
        }
        header("Location: index.php?waiter=" . $_REQUEST['waiter'] . "&table=" . $tableVar . "&tablePart=" . $_REQUEST['tablePart']);
        exit();
    } else {
        $tableVar = $_REQUEST['table'];
    }
    $tablePartVar = $_REQUEST['tablePart'];
    $waiterName = "";
}
//if(isset($_REQUEST['tablePart']) && $_REQUEST['tablePart'] == 'C' )
//{
//  $selectTableForCar = "SELECT MAX(tableId) AS tableId 
//                           FROM ordermaster 
//                         WHERE tablePart = 'C'";
//  $selectTableForCarRes = mysql_query($selectTableForCar);
//  $selectTableForCarRow = mysql_fetch_array($selectTableForCarRes);
//
//  $tableVar = $selectTableForCarRow['tableId'] + 1;
//  if($tableVar < 36)
//  {
//  $tableVar = 37;
//  }
//}


$cmd = "SELECT * FROM item ORDER BY itemSeq ASC";
$itemsArrayResult = mysql_query($cmd) or die("Item Selection Error : " . mysql_error());
$itemsArray = array();
$i = 0;
while ($row = mysql_fetch_array($itemsArrayResult)) {
    if ($row['visibled'] != '0') {
        $itemsArray[$i]['itemId'] = $row['itemId'];
        $itemsArray[$i]['itemName'] = $row['itemName'];
        $itemsArray[$i]['itemPrice'] = $row['itemPrice'];
        $itemsArray[$i]['itemSeq'] = $row['itemSeq'];
        $itemsArray[$i]['callJs'] = $row['callJs'];
        $itemsArray[$i]['itemPhoto'] = $row['itemPhoto'];
        $itemsArray[$i]['backColor'] = $row['backColor'];
        $i++;
    }
}


include("./bottom.php");
$smarty->assign('itemsArray', $itemsArray);
$smarty->assign('tableVar', $tableVar);
$smarty->assign('waiterVar', $waiterVar);
$smarty->assign('tablePartVar', $tablePartVar);
$smarty->assign('waiterName', $waiterName);
$smarty->assign('selectTableForCarRow', $selectTableForCarRow);
$smarty->display('index.tpl');
?>