<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include("include/config.inc.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page
  function pageHeader()
  {
    global $pdf;
    $pdf->Image('./images/om.jpg',10,2,40,22);
    $pdf->SetFont('Arial','',10);
    
    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Times','',35);
    $pdf->SetXY(5,3);
    $pdf->Cell(100, 120, '', 1, 0, 'C');
    
  	$pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Times','',35);
    $pdf->SetXY(5,3);
    $pdf->Cell(50, 20, '', 1, 0, 'C');
    
    $pdf->SetFont('Times','',20);
    $pdf->SetXY(55,3);
    $pdf->Cell(25, 10, 'Waiter', 1, 0, 'C');
    $pdf->Cell(25, 10, 'Table', 1, 0, 'C');
    
    $pdf->SetXY(55,13);
    $pdf->Cell(25, 10, '', 1, 0, 'C');
    $pdf->Cell(25, 10, '', 1, 0, 'C');
    
    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(5,25);
    $pdf->Cell(12, 6, 'Sr.No.', 1, 0, 'C', 0);
    $pdf->Cell(15, 6, 'Qty.',   1, 0, 'C', 0);
    $pdf->Cell(27, 6, 'Item.',  1, 0, 'C', 0);
    $pdf->Cell(24, 6, 'Rate',   1, 0, 'C', 0);
    $pdf->Cell(22, 6, 'Amount', 1, 0, 'C', 0);
  }
  pageHeader();
  $i         = 0; 
  $rowHeight = 5;
  $yAxis     = 18;
  $orderId       = isset($_REQUEST['orderId']) ? $_REQUEST['orderId']   : 0;
  $tableId       = isset($_REQUEST['tableId']) ? $_REQUEST['tableId']   : 0;
  $waiterId      = isset($_REQUEST['waiterId']) ? $_REQUEST['waiterId'] : 0;
  $tablePart     = isset($_REQUEST['tablePart']) ? $_REQUEST['tablePart'] : 0;
  $totalamount   = 0;
  $totalquantity = 0;
  $selectOrder = "SELECT itemName,itemPrice,quantity,orderTime,ordereditems.ordereditemsId ,ordereditems.orderId,
                         waiterName,ordermaster.tableId,ordereditems.itemId,waiter.waiterId,ordermaster.tableId,
                         SUM( ordereditems.quantity * item.itemPrice ) AS totalamount, sum( quantity) AS totalItem
                    FROM ordereditems 
               LEFT JOIN ordermaster ON ordereditems.orderId = ordermaster.orderId
               LEFT JOIN item ON ordereditems.itemId =  item.itemId
               LEFT JOIN waiter ON ordermaster.waiterId = waiter.waiterId
                   WHERE ordermaster.tableId = ".$tableId."
                     AND ordermaster.waiterId = ".$waiterId."
                     AND ordermaster.orderstatus = 'L'
                     AND ordermaster.tablePart = '".$tablePart;
  $selectOrderRes = mysql_query($selectOrder);
  $updateOrdredItems = "UPDATE ordermaster 
                           SET orderstatus = 'C'
                         WHERE orderId = ".$orderId;
  mysql_query($updateOrdredItems);
  while($orderRow = mysql_fetch_array($selectOrderRes))
  {
    $totalamount   += $orderRow['totalamount'];
    $totalquantity += $orderRow['quantity'];
      if ($i == 31)
      {
        $pdf->AddPage();
        pageHeader();
        grnmasterFunc();
        //Go to next row
        $yAxis = 75;
  
        //Set $i variable to 0 (first row)
        $i = 1;
      }
    global $tableRow;
    $pdf->SetXY(60,10);
    $pdf->SetFont('Arial','B',12);
    $pdf->Write(15,' '.($orderRow['waiterName']));
    $pdf->SetXY(90,10);
    $pdf->SetFont('Arial','B',12);
    $pdf->Write(15,' '.($orderRow['tableId']));
    $pdf->SetXY(10,$yAxis);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(35,($i+1));
    $pdf->SetXY(22,$yAxis);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(35,($orderRow['quantity']));
    $pdf->SetXY(33,$yAxis);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(35,($orderRow['itemName']));
    $pdf->SetXY(68,$yAxis);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(35,($orderRow['itemPrice']));
    $pdf->SetXY(90,$yAxis);
    $pdf->SetFont('Arial','',10);
    $pdf->Write(35,($orderRow['quantity'] * $orderRow['itemPrice']));
    $yAxis = $yAxis + $rowHeight;
    $i = $i + 1;
  } 
  $pdf->SetXY(89,$yAxis);
  $pdf->SetFont('Arial','',10);
  $pdf->Write(145,''.($totalamount));
  $pdf->SetXY(22,$yAxis);
  $pdf->SetFont('Arial','',10);
  $pdf->Write(145,''.($totalquantity));
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', '', 10);
  $pdf->SetXY(5,117);
  $pdf->Cell(12, 6, 'TOTAL', 1, 0, 'C', 0);
  $pdf->Cell(15, 6, '', 1, 0, 'C', 0);
  $pdf->Cell(51, 6, '', 1, 0, 'C', 0);
  $pdf->Cell(22, 6, '', 1, 0, 'C', 0);
  $pdf->Output();
}
?>