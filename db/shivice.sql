-- phpMyAdmin SQL Dump
-- version 4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2015 at 05:58 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shivice`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` int(11) NOT NULL,
  `itemPrice` int(11) NOT NULL DEFAULT '0',
  `kgPrice` int(11) NOT NULL DEFAULT '0',
  `itemName` varchar(100) NOT NULL,
  `itemSeq` int(5) NOT NULL,
  `itemPhoto` varchar(200) NOT NULL,
  `visibled` int(1) NOT NULL,
  `callJs` varchar(20) NOT NULL,
  `backColor` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `itemPrice`, `kgPrice`, `itemName`, `itemSeq`, `itemPhoto`, `visibled`, `callJs`, `backColor`) VALUES
(2, 40, 400, 'Ice Cream', 56, './images/item_2.jpg', 1, 'setIceCreamItemId', '#BFB110'),
(3, 60, 600, 'Shiv Special', 41, './images/item_3.jpg', 1, 'setIceCreamItemId', '#BFB110'),
(4, 30, 0, 'King Cone', 40, './images/item_4.jpg', 1, 'setItemId', '#8BBA30'),
(5, 25, 0, 'Candy', 39, './images/item_5.jpg', 1, 'setItemId', '#BEA881'),
(6, 35, 0, 'Sp. Mava Candy', 38, './images/item_6.jpg', 1, 'setItemId', '#BEA881'),
(7, 100, 0, 'Sundae', 37, './images/item_7.jpeg', 1, 'setItemId', '#FFB700'),
(8, 50, 0, 'Juice', 36, './images/item_8.jpg', 1, 'setItemId', '#E768AB'),
(9, 70, 0, 'Faluda', 35, './images/item_9.jpg', 1, 'setItemId', '#02753C'),
(10, 45, 0, 'Chickoo Pl.', 34, './images/item_10.jpg', 1, 'setItemId', '#38BEB7'),
(11, 75, 0, 'Vanila/Coffee Pl.', 33, './images/item_11.jpg', 1, 'setItemId', '#38BEB7'),
(12, 95, 0, 'Shake Pl.', 32, './images/item_12.jpg', 1, 'setItemId', '#38BEB7'),
(13, 140, 0, 'Shiv Sp. Pl.', 31, './images/item_13.jpg', 1, 'setItemId', '#38BEB7'),
(14, 120, 0, 'Noodles', 42, './images/item_14.jpg', 1, 'setItemId', '#D6552D'),
(15, 140, 0, 'Mush/Manch N.', 43, './images/item_15.jpg', 1, 'setItemId', '#D6552D'),
(16, 120, 0, 'Rice/Bhel', 44, './images/item_16.jpeg', 0, 'setItemId', '#FFFFFF'),
(17, 140, 0, 'Pan/Mus Rice', 55, './images/item_17.jpg', 1, 'setItemId', '#647687'),
(18, 120, 0, 'M. Dry/Gravy', 54, './images/item_18.jpeg', 1, 'setItemId', '#7A3B3F'),
(19, 180, 0, 'Pan.Ch.Gravy/Dry', 53, './images/item_19.jpeg', 0, 'setItemId', '#E7E7E7'),
(20, 190, 0, 'Trip. Sch. Rice', 52, './images/item_20.jpg', 1, 'setItemId', '#647687'),
(21, 150, 0, 'Pizza', 51, './images/item_21.jpg', 1, 'setItemId', '#D0A9F5'),
(22, 19, 190, 'Shiv Spacial Pizza', 50, './images/item_22.jpg', 1, 'setIceCreamItemId', '#D0A9F5'),
(23, 130, 0, 'Club', 49, './images/item_23.jpg', 1, 'setItemId', '#E3AD27'),
(24, 90, 0, 'Grill', 48, './images/item_24.jpg', 1, 'setItemId', '#E3AD27'),
(25, 85, 0, 'Non Grill', 47, './images/item_25.jpg', 1, 'setItemId', '#E3AD27'),
(26, 130, 0, 'Oven Tost', 46, './images/item_26.jpg', 1, 'setItemId', '#04ACAD'),
(27, 80, 0, 'Garlic Bread', 45, './images/item_27.jpeg', 1, 'setItemId', '#04ACAD'),
(28, 20, 0, 'Sevpuri', 30, './images/item_28.jpg', 1, 'setItemId', '#C8FF80'),
(29, 25, 0, 'Drink', 29, './images/item_29.jpeg', 1, 'setItemId', '#BA426F'),
(30, 20, 0, 'Water', 28, './images/item_30.gif', 1, 'setItemId', '#BA426F'),
(31, 30, 0, 'Extra Dry Fruits', 13, './images/item_31.jpg', 0, 'setItemId', '#E7E7E7'),
(32, 40, 0, 'Ex. Topping', 12, './images/item_32.jpg', 1, 'setItemId', '#F4D40D'),
(33, 70, 0, 'Bread Non Grill', 11, './images/item_48.jpg', 1, 'setItemId', '#FFFFFF'),
(34, 30, 0, 'Man. Chopsuey', 10, './images/item_34.jpg', 1, 'setItemId', '#F4D40D'),
(35, 10, 0, 'Chutney', 9, './images/item_35.jpg', 1, 'setItemId', '#F4D40D'),
(36, 50, 0, 'Chickoo Sp.', 8, './images/item_36.jpg', 1, 'setItemId', '#BAB9A9'),
(37, 80, 0, 'Vanila/Coffee Sp.', 7, './images/item_37.jpg', 1, 'setItemId', '#BAB9A9'),
(38, 100, 0, 'Shake Sp.', 6, './images/item_38.jpg', 1, 'setItemId', '#BAB9A9'),
(39, 150, 0, 'Shiv Sp. Sp.', 5, './images/item_39.jpg', 1, 'setItemId', '#BAB9A9'),
(40, 85, 0, 'Burger', 3, './images/item_40.jpeg', 1, 'setItemId', '#CF2027'),
(41, 80, 0, 'French Fries', 4, './images/item_41.jpeg', 1, 'setItemId', '#F6CEE3'),
(42, 18, 180, 'Ice-Cream Half', 14, './images/item_42.jpg', 1, 'setIceCreamItemId', '#BFB110'),
(43, 20, 200, 'Ice-Cream Half', 15, './images/item_43.jpg', 1, 'setIceCreamItemId', '#BFB110'),
(44, 40, 300, 'Ice-Cream Half', 16, './images/item_44.jpg', 1, 'setIceCreamItemId', '#BFB110'),
(45, 0, 350, 'Ice Cream', 27, './images/item_45.jpg', 1, 'setIceCreamItemId', '#E7E7E7'),
(46, 0, 400, 'Ice Cream 1 kg', 26, './images/item_46.jpg', 0, 'setIceCreamItemId', '#E7E7E7'),
(47, 75, 0, 'Bread Grill', 25, './images/item_49.jpg', 1, 'setItemId', '#7A3B3F'),
(58, 0, 0, 'Sev PUri', 24, './images/item_58.jpg', 1, 'setItemId', '#FFFFFF'),
(59, 0, 0, 'Sev PUri 2', 23, '', 1, 'setItemId', '#FFFFFF'),
(60, 0, 0, 'Sev PUri 3', 22, '', 1, 'setItemId', '#FFFFFF'),
(61, 0, 0, 'Sev PUri 4', 21, '', 1, 'setItemId', '#FFFFFF'),
(62, 0, 0, 'Sev PUri 5', 20, '', 1, 'setItemId', '#FFFFFF'),
(63, 0, 0, 'Sev PUri 6', 19, '', 1, 'setItemId', '#FFFFFF'),
(64, 0, 0, 'Sev PUri 7', 18, '', 1, 'setItemId', '#FFFFFF'),
(65, 0, 0, 'Sev PUri 8', 17, '', 1, 'setItemId', '#FFFFFF'),
(66, 25, 250, 'Sev PUri 9', 2, '', 1, 'setItemId', '#FFFFFF'),
(67, 25, 0, 'Sev[iro 100', 1, '', 1, 'setItemId', '#CF2027'),
(68, 25, 0, 'Sev[iro 101', 57, '', 1, 'setItemId', '#CF2027'),
(69, 0, 0, 'Sev[iro 102', 58, '', 1, 'setIceCreamItemId', '#CF2027'),
(70, 20, 200, 'Rajbhog Ice cream', 59, './images/item_70.jpg', 1, 'setIceCreamItemId', '#FFFFFF');

-- --------------------------------------------------------

--
-- Table structure for table `ordereditems`
--

CREATE TABLE IF NOT EXISTS `ordereditems` (
  `orderedItemsId` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  `orderTime` time DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `parcel` varchar(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordereditems`
--

INSERT INTO `ordereditems` (`orderedItemsId`, `orderId`, `itemId`, `quantity`, `weight`, `orderTime`, `userName`, `parcel`) VALUES
(1, 1, 60, 3, 0, '10:42:31', 'om', ''),
(2, 1, 44, 3, 0, '10:42:36', 'om', ''),
(3, 1, 29, 5, 0, '10:42:44', 'om', ''),
(4, 2, 3, 1, 0, '10:43:00', 'om', ''),
(5, 2, 34, 1, 0, '10:43:33', 'om', ''),
(9, 2, 24, 3, 0, '11:02:13', 'om', ''),
(10, 2, 2, 1, 750, '11:05:22', 'om', ''),
(11, 4, 66, 1, 0, '11:07:25', 'om', ''),
(12, 4, 40, 1, 0, '11:07:26', 'om', ''),
(13, 4, 39, 1, 0, '11:07:27', 'om', ''),
(14, 4, 41, 1, 0, '11:07:29', 'om', ''),
(15, 4, 38, 1, 0, '11:07:30', 'om', ''),
(16, 4, 37, 1, 0, '11:07:32', 'om', ''),
(17, 4, 36, 1, 0, '11:07:33', 'om', ''),
(18, 4, 33, 1, 0, '11:07:35', 'om', ''),
(19, 4, 42, 2, 0, '11:07:39', 'om', ''),
(20, 4, 45, 3, 0, '11:07:43', 'om', ''),
(21, 4, 5, 2, 0, '11:07:45', 'om', ''),
(22, 4, 23, 8, 0, '11:07:49', 'om', ''),
(23, 4, 24, 1, 0, '11:07:48', 'om', 'p'),
(24, 4, 18, 2, 0, '11:07:53', 'om', ''),
(25, 4, 2, 1, 750, '11:07:55', 'om', ''),
(26, 4, 8, 1, 0, '11:07:59', 'om', ''),
(27, 4, 8, 2, 0, '11:08:04', 'om', ''),
(28, 2, 2, 1, 0, '12:22:17', 'om', ''),
(29, 2, 2, 1, 1000, '12:55:42', 'om', ''),
(30, 2, 23, 1, 0, '13:42:52', 'om', ''),
(31, 4, 43, 1, 500, '13:43:15', 'om', ''),
(32, 4, 43, 3, 0, '13:43:20', 'om', 'p'),
(33, 4, 43, 1, 1000, '13:43:24', 'om', 'p'),
(34, 5, 41, 1, 0, '16:26:27', 'om', ''),
(35, 5, 38, 1, 0, '16:26:50', 'om', ''),
(36, 1, 70, 3, 0, '16:52:27', 'om', ''),
(37, 1, 70, 1, 750, '16:52:39', 'om', ''),
(38, 6, 33, 1, 0, '16:52:55', 'om', ''),
(39, 6, 67, 1, 0, '16:53:02', 'om', ''),
(40, 7, 44, 1, 0, '16:53:15', 'om', ''),
(41, 6, 67, 1, 0, '16:53:23', 'om', ''),
(42, 1, 70, 1, 750, '18:11:38', 'om', ''),
(43, 1, 43, 1, 0, '16:33:50', 'om', '');

-- --------------------------------------------------------

--
-- Table structure for table `ordermaster`
--

CREATE TABLE IF NOT EXISTS `ordermaster` (
  `orderId` int(11) NOT NULL,
  `waiterId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  `tablePart` varchar(5) NOT NULL DEFAULT 'A',
  `startTime` time NOT NULL,
  `startDate` date DEFAULT NULL,
  `orderstatus` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordermaster`
--

INSERT INTO `ordermaster` (`orderId`, `waiterId`, `tableId`, `tablePart`, `startTime`, `startDate`, `orderstatus`) VALUES
(1, 7, 8, 'A', '10:42:27', '2015-02-02', 'L'),
(2, 7, 12, 'A', '10:43:00', '2015-02-02', 'R'),
(3, 16, 2, 'A', '10:43:40', '2015-02-02', 'L'),
(4, 10, 9, 'A', '11:07:25', '2015-02-02', 'C'),
(5, 8, 2, 'B', '16:26:27', '2015-02-02', 'L'),
(6, 1, 1, 'A', '16:52:55', '2015-02-02', 'L'),
(7, 1, 3, 'A', '16:53:15', '2015-02-02', 'L');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL,
  `userName` varchar(500) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `userType` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `userName`, `password`, `userType`) VALUES
(1, 'om', 'om', 'admin'),
(2, 'om1', 'om1', 'local');

-- --------------------------------------------------------

--
-- Table structure for table `waiter`
--

CREATE TABLE IF NOT EXISTS `waiter` (
  `waiterId` int(11) NOT NULL,
  `waiterName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waiter`
--

INSERT INTO `waiter` (`waiterId`, `waiterName`) VALUES
(1, 'T'),
(2, 'N'),
(3, 'G'),
(4, 'A'),
(5, 'J'),
(6, 'D'),
(7, 'V'),
(8, 'B'),
(9, 'F'),
(10, 'O'),
(11, 'M'),
(12, 'L'),
(13, 'U'),
(14, 'H'),
(15, 'K'),
(16, 'P');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `ordereditems`
--
ALTER TABLE `ordereditems`
  ADD PRIMARY KEY (`orderedItemsId`);

--
-- Indexes for table `ordermaster`
--
ALTER TABLE `ordermaster`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `waiter`
--
ALTER TABLE `waiter`
  ADD PRIMARY KEY (`waiterId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `ordereditems`
--
ALTER TABLE `ordereditems`
  MODIFY `orderedItemsId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `ordermaster`
--
ALTER TABLE `ordermaster`
  MODIFY `orderId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `waiter`
--
ALTER TABLE `waiter`
  MODIFY `waiterId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
