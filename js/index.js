var itemIdVar="";
var currentOrderedItemsId="";
var quantityPlusMinus = "null";
var weightNew = "null";
  function setWeight(weight)
  {
    weightNew = weight;

    if(weightNew == 250 )
    {
      $("#250").css('background-color','red')
      $("#500").css('background-color','blue')
      $("#750").css('background-color','blue')
      $("#1000").css('background-color','blue')
    }
    else if(weightNew == 500 )
    {
      $("#500").css('background-color','red')
      $("#250").css('background-color','blue')
      $("#750").css('background-color','blue')
      $("#1000").css('background-color','blue')
    }
    else if(weightNew == 750 )
    {
      $("#750").css('background-color','red')
      $("#250").css('background-color','blue')
      $("#500").css('background-color','blue')
      $("#1000").css('background-color','blue')
    }
    else if(weightNew == 1000 )
    {
      $("#1000").css('background-color','red')
      $("#250").css('background-color','blue')
      $("#500").css('background-color','blue')
      $("#750").css('background-color','blue')
    }
  }
  function setItemId(item,waiterVar,tableVar,tablePartVar)
  {
  	
    itemIdVar        = (item.id);
    var waiterVar    = waiterVar;
    var tableVar     = tableVar;
    var tablePartVar = tablePartVar;
    var dataString   = 'itemIdVar='+itemIdVar+'&waiterVar='+ waiterVar + '&tableVar=' + tableVar + '&tablePartVar=' + tablePartVar;
    $.ajax({
      url:'setItemAjax.php',
      dataType:'HTML',
      data: dataString,
      type:'POST',
      success:function(msg)
      {
        $('.rightable').html(msg);
        document.getElementById("carIfrem").src = document.getElementById("carIfrem").src;
        $("#250").css('background-color','blue')
        $("#500").css('background-color','blue')
        $("#750").css('background-color','blue')
        $("#1000").css('background-color','blue')        
      }
    });    
  }
  function setIceCreamItemId(item,waiterVar,tableVar,tablePartVar)
  {
    itemIdVar = (item.id);
    var waiterVar    = waiterVar;
    var tableVar     = tableVar;
    var tablePartVar = tablePartVar;
    var dataString   = 'itemIdVar='+ itemIdVar  + '&waiterVar='+ waiterVar + '&tableVar=' + tableVar + '&tablePartVar=' + tablePartVar+ '&weight=' +weightNew;
    $.ajax({
      url:'setIceCreamAjax.php',
      dataType:'HTML',
      data: dataString,
      type:'POST',
      success:function(msg)
      {      	
        $('.rightable').html(msg);        
        document.getElementById("carIfrem").src = document.getElementById("carIfrem").src;
        if(weightNew != 1000)
        {
            weightNew = "null";
        }

        $("#250").css('background-color','blue')
        $("#500").css('background-color','blue')
        $("#750").css('background-color','blue')
        $("#1000").css('background-color','blue')
      }
    });
  }
  function setQuntityPlusMinus(minusOrPlus)
  {
    quantityPlusMinus = minusOrPlus.id;
    if(quantityPlusMinus == "minus")
    {
      $('#minus').css("background-color","red");
      $('#plus').css("background-color","#B3AFAF");
    }
    if(quantityPlusMinus == "plus")
    {
      $('#minus').css("background-color","#B3AFAF");
      $('#plus').css("background-color","red");
    }
  }
  
  function addQuantity(quantity,waiterVar,tableVar,tablePartVar)
  {
    var quantity    = quantity.value;
    var waiter      = waiterVar;
    var table       = tableVar;
    var tableaPart  = tablePartVar;
    var itemId      = itemIdVar;
    var plusOrMinus = quantityPlusMinus;
    var dataString  = 'quantity=' + quantity + '&currentOrderedItemsId='+ currentOrderedItemsId  + '&waiterVar='+ waiterVar + '&tableVar=' + tableVar + '&tablePartVar=' + tablePartVar + '&plusOrMinusVar=' + plusOrMinus ;
    $.ajax({
      url:'addQuntityAjax.php',
      dataType:'HTML',
      data:dataString,
      type:'POST',
      success:function(msg)
      {
        $('.rightable').html(msg);
        quantityPlusMinus = "null";
        document.getElementById("carIfrem").src = document.getElementById("carIfrem").src;
        $('#plus').css("background-color","#B3AFAF");
        $('#minus').css("background-color","#B3AFAF");
      }
    });
  }
  function changeUrlTable(waiterExp,tableNoExp,tablePartExp)
  {
    window.location.href="index.php?waiter="+waiterExp+"&table="+tableNoExp+"&tablePart="+tablePartExp; 
  }
  function changeUrlCar(waiterExp,tableNoExp,tablePartExp)
  {
    window.location.href="index.php?changeCar=1&waiter="+waiterExp+"&table="+tableNoExp+"&tablePart="+tablePartExp;
  }
  function changeUrlWaiter(waiterExp,tableNoExp,tablePartExp)
  {
    window.location.href="index.php?waiter="+waiterExp+"&table="+tableNoExp+"&tablePart="+tablePartExp;
  }
  $(document).ready(function(){
    if(quantityPlusMinus == "plus")
    {
      $('#plus').css("background-color","red");
    }
    $(".items").click(function(){
      $(".items").bind('click', function (e) {
        $("#box2").show();
        $("#box2").css("left", e.pageX);
        $("#box2").css("top", e.pageY);
      });  
    });
    $('#close').click(function(){
      $("#box2").hide();
    });
    $('.btnWithout').click(function(){
      $('.with').hide();
      $('.without').show();
      $('.btnWithout').css("background-color","red");
      $('.btnWith').css("background-color","white");
    });
    $('.btnWith').click(function(){
      $('.with').show();
      $('.without').hide();
      $('.btnWith').css("background-color","red");
      $('.btnWithout').css("background-color","white");
    });
    $("#home").click(function(){
      window.location.href="index.php?waiter=1&table=1&tablePart=A";
    });
    
  });
  function generateCarOrder(a)
  {
    window.parent.location.href="setUrlCar.php?orderId="+a; 
  }
  function printFun(orderIdVar)
  {
    var url = "amountListPdf1.php?orderId="+orderIdVar;
    var win = window.open("amountListPdf1.php?orderId="+orderIdVar);
    win.window.print();
    setTimeout( function() { 
    win.close();
    setTimeout("window.parent.reload(true);", 500);
    }, 500);
  }