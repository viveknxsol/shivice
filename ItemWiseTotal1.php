<?php
include("include/config.inc.php");
if(!isset($_SESSION['s_activId']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
  header("Location:checkLogin.php");
}
if(isset($_REQUEST['waiter']))
{
  $waiterVar = $_REQUEST['waiter'] ;
}
else
{
	$waiterVar = 0 ;
}

if(isset($_REQUEST['table']))
{
  $tableVar = $_REQUEST['table'] ;
}
else
{
	$tableVar = 0 ;
}
if(isset($_REQUEST['tablePart']))
{
  $tablePartVar = $_REQUEST['tablePart'] ;
}
else
{
	$tablePartVar = "A" ;
}
  
$orderstatus = "";
$joinResult = array();
$i=0;
$allTablesSelected = isset($_POST['allTables']) ? $_POST['allTables'] : 'Closed Tables';
$joinOrderMasterOrderedItems = " SELECT waiter.waiterName, waiter.waiterId, ordermaster.tablePart,ordermaster.orderId,ordermaster.startDate,
                                        ordermaster.tableId, ordermaster.orderstatus,                                        
                                        COUNT( * ) AS totalItem, ordermaster.startTime, 
                                        MAX( ordereditems.orderTime ) AS lastordertime
                                   FROM ordermaster
                                   JOIN ordereditems ON ordermaster.orderId = ordereditems.orderId
                                   JOIN item ON item.itemId = ordereditems.itemId
                                   JOIN waiter ON waiter.waiterId = ordermaster.waiterId
                                  WHERE 1 = 1";
if(isset($_POST['allTables']) && $_POST['allTables'] == 'Closed Tables')
{
   $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'C'";
}
else if(isset($_POST['allTables']) && $_POST['allTables'] == 'Live Tables')
{
	 $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'L'";
}
else if(isset($_POST['allTables']) && $_POST['allTables'] == 'Received Tables')
{
	$joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'R'";
}
if($allTablesSelected == 'Closed Tables')
{
	$joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'C'";
}
$joinOrderMasterOrderedItems .= " GROUP BY ordermaster.orderId ORDER BY ordermaster.startDate,ordermaster.startTime";

$joinOrderMasterOrderedItemsRes = mysql_query($joinOrderMasterOrderedItems) or die(mysql_error());
$grandTotal=0;
$grandTotalItems=0;
while($joinOrderMasterOrderedItemsRow = mysql_fetch_array($joinOrderMasterOrderedItemsRes))
{
	 $joinResult[$i]['orderId']       = $joinOrderMasterOrderedItemsRow['orderId'];
	 $joinResult[$i]['waiterName']    = $joinOrderMasterOrderedItemsRow['waiterName'];
	 $joinResult[$i]['waiterId']      = $joinOrderMasterOrderedItemsRow['waiterId'];
	 $joinResult[$i]['tableId']       = $joinOrderMasterOrderedItemsRow['tableId'];
	 $joinResult[$i]['tablePart']     = $joinOrderMasterOrderedItemsRow['tablePart'];
	 $joinResult[$i]['totalamount']   = 0;
	 $joinResult[$i]['totalItem']     = $joinOrderMasterOrderedItemsRow['totalItem'];
         $grandTotalItems+= $joinOrderMasterOrderedItemsRow['totalItem'];
	 $joinResult[$i]['startTime']     = $joinOrderMasterOrderedItemsRow['startTime'];
	 $joinResult[$i]['lastordertime'] = $joinOrderMasterOrderedItemsRow['lastordertime'];
	 $joinResult[$i]['tablePart']     = $joinOrderMasterOrderedItemsRow['tablePart'];
	 $joinResult[$i]['orderstatus']   = $joinOrderMasterOrderedItemsRow['orderstatus'];
	 $forTotalAmount = " SELECT ordereditems.quantity , item.itemPrice,ordereditems.weight 
                                   FROM ordereditems
                                   JOIN item ON item.itemId = ordereditems.itemId
                                  WHERE ordereditems.orderId = ".$joinResult[$i]['orderId'];
$forTotalAmountRes = mysql_query($forTotalAmount) or die (mysql_error());
$forTotalAmountArray=array();
$j=0;
while($forTotalAmountRow = mysql_fetch_array($forTotalAmountRes))
{
  if($forTotalAmountRow['weight']== 0)
	 {
	   $forTotalAmountArray[$j]['amount']   =  $forTotalAmountRow['itemPrice'] * $forTotalAmountRow['quantity'];
	 }
	 else
	 {
	 	 $forTotalAmountArray[$j]['amount']   =  ceil((($forTotalAmountRow['itemPrice'] * $forTotalAmountRow['weight'])/1000)*$forTotalAmountRow['quantity']);
	 }
	 $joinResult[$i]['totalamount']             += $forTotalAmountArray[$j]['amount'];
         $grandTotal += $forTotalAmountArray[$j]['amount'];
	 $j++;
}
	 $i++;
}
$join2Result = array();
$l=0;
//$allTablesSelected = isset($_POST['allTables']) ? $_POST['allTables'] : 'Closed Tables';

$joinItemOrderedItems = "SELECT ordereditems.quantity, COUNT( * ) AS totalItem2,item.itemId, item.itemName,item.itemPrice,ordereditems.orderId
								FROM ordereditems
								JOIN item ON item.itemId = ordereditems.itemId
								GROUP BY ordereditems.itemId
								";

$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
$sum=0;
$grandTotal2=0;
$grandTotalItems2=0;
while($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes))
{
	 $join2Result[$l]['orderId']		  = $joinItemOrderedItemsRow['orderId'];
	 $join2Result[$l]['quantity']	  = $joinItemOrderedItemsRow['quantity'];
	 $join2Result[$l]['itemId']		  = $joinItemOrderedItemsRow['itemId'];
	 $join2Result[$l]['itemName'] 	  = $joinItemOrderedItemsRow['itemName'];
	 $join2Result[$l]['itemPrice']	  = $joinItemOrderedItemsRow['itemPrice'];
	 $join2Result[$l]['totalamount2']   = 0;
	 $join2Result[$l]['totalItem2']     = $joinItemOrderedItemsRow['totalItem2'];
	 $join2Result[$l]['totalItemAmount2'] = $joinItemOrderedItemsRow['totalItem2'] * $joinItemOrderedItemsRow['itemPrice'] ;
     $grandTotalItems2+= $joinItemOrderedItemsRow['totalItem2'];
	 $sum += $join2Result[$l]['totalItemAmount2'];
	 $forItemTotalAmount = " SELECT DISTINCT(item.itemId), ordereditems.quantity, COUNT(*) AS totalItems2, item.itemPrice,ordereditems.weight 
                                  FROM ordereditems
                                  JOIN item ON item.itemId = ordereditems.itemId
								  ";
$forItemTotalAmountRes = mysql_query($forItemTotalAmount) or die (mysql_error());
$forItemTotalAmountArray=array();
$k=0;
while($forItemTotalAmountRow = mysql_fetch_array($forItemTotalAmountRes))
{
  if($forItemTotalAmountRow['weight']== 0)
	 {
	   $forItemTotalAmountArray[$k]['amount2']   =  $forItemTotalAmountRow['itemPrice'] * $forItemTotalAmountRow['totalItems2'];
	 }
	 else
	 {
	 	 $forItemTotalAmountArray[$k]['amount2']   =  ceil((($forItemTotalAmountRow['itemPrice'] * $forItemTotalAmountRow['weight'])/1000)*$forItemTotalAmountRow['quantity']);
	 }
	 $join2Result[$l]['totalamount2']             += $forItemTotalAmountArray[$k]['amount2'];
         $grandTotal2 += $forItemTotalAmountArray[$k]['amount2'];
		 
	 $k++;
}
	 $l++;
}								

$tableStausValueArr[0]  = "Closed Tables";
$tableStausOutputArr[0] = "Closed Tables";
$tableStausValueArr[1]  = "Live Tables";
$tableStausOutputArr[1] = "Live Tables";
$tableStausValueArr[2]  = "Received Tables";
$tableStausOutputArr[2] = "Received Tables";

include("./bottom.php");
$smarty->assign('tableVar',$tableVar);
$smarty->assign('sum',$sum);
$smarty->assign('grandTotal',$grandTotal);
$smarty->assign('grandTotalItems',$grandTotalItems);
$smarty->assign('grandTotal2',$grandTotal2);
$smarty->assign('grandTotalItems2',$grandTotalItems2);
$smarty->assign('userType',$_SESSION['s_userType']);
$smarty->assign('tablePartVar',$tablePartVar);
$smarty->assign('waiterVar',$waiterVar);
$smarty->assign('joinResult',$joinResult);
$smarty->assign('join2Result',$join2Result);
$smarty->assign('allTablesSelected',$allTablesSelected);
$smarty->assign('tableStausValueArr',$tableStausValueArr);
$smarty->assign('tableStausOutputArr',$tableStausOutputArr);
$smarty->assign('orderstatus',$orderstatus);

$smarty->display('TwoTables.tpl');

?>
