{extends file="main.tpl"}
{block name=head}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="./media/js/jquery.js" type="text/javascript"></script>
    <script src="./media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script>
        function printFun(orderIdVar)
        {
            window.open(amountListPdf1.php?orderId = + orderIdVar);
        }
    </script>
    <style type="text/css">
        @import "./media/css/demo_table_jui.css";
        @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
    </style>

    <style>
        *{
            font-family: arial;
        }

        .big {
            margin-left:20px;
            border: 1px solid black;
            width:100px;
            height:100px;
            font-size:20px;
            background-color:#F3F3EE;
            cursor:pointer;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#datatables').dataTable({
                "sPaginationType": "full_numbers",
                "aaSorting": [[2, "desc"]],
                "bJQueryUI": true
            });
        })

        function checkAll(status)
        {
            var checkbox = document.getElementsByName('CloseTableChk[]');
            for (i = 0; i < checkbox.length; i++)
            {
                checkbox[i].checked = status;
            }
        }

        function trunkFun()
        {
            alert("hiii");
            window.parent.location.href = "deleteFromClose.php?trunk='true'";

        }
    </script>

{/block}
{block name=body}
    <form name="tables" method="post" action="{$smarty.server.PHP_SELF}">
        <table align="center" style="background-color:#F7B64A; box-shadow: 10px 10px 5px #888888; height: 5em;width: 20em; -moz-border-radius: 1em 4em 1em 4em; border-radius: 1em 4em 1em 4em;"  border="0" cellpadding="2" cellspacing="2" id="tableNo" width="50%" class="waiterdiv" >
            <tr>
                <td align="center" valign="top"><font size="7px">Order Master</b></td>
            </tr>
            <tr>
                <td align="center">
                    <select name="allTables" id="allTables" autofocus="autofocus" onchange="this.form.submit();">
                        <option value="0">All Table</option>
                        {html_options values=$tableStausValueArr output=$tableStausOutputArr selected=$allTablesSelected}
                    </select>
                </td>
            </tr>
        </table>
    </form>
    <div><br />
        <form method="post" action="deleteFromClose.php">
            <table id="datatables" class="display">
                </tfoot>	  
					<thead>
                    <tr>
						{if $userType == "admin"}
                            <th><input type="checkbox" name="CloseTableChkAll" onclick="checkAll(this.checked);"></th>
                            <th><font size="4px">Payment</font></th>
                            {/if}
                        <th><font size="5px">Item Id</font></th>
                        <th><font size="5px">Item Name</font></th>
                        <th><font size="5px">Total Items</font></th>
                        <th><font size="5px">Item Price</font></th>
                        <th><font size="5px">Total Item Amount</font></th>
                    </tr>
                </thead>
                <tbody>
                    {section name="sec" loop=$joinResult} 
                        <tr class="{cycle values="odd,even"}">
                            {if $userType == "admin"}
                                <td><input type="checkbox" name="CloseTableChk[]" value="{$joinResult[sec].orderId}"/></td>	
								<td><a href="receive.php?orderId={$joinResult[sec].orderId}" name="rec">
                                        {if {$joinResult[sec].orderstatus} eq 'R'}<font size="6px" color="green">Received</font></a>
                                        {else if {$joinResult[sec].orderstatus} eq 'L'}<font size="6px" color="green">Live</font>{else}<font size="6px" color="green">Closed</font>{/if}
                                    </td>
                               {/if}
                                <td align="right"><font  size="6px" color="red"> {$joinResult[sec].itemId}</font></td>
                                <td align="right"><font  size="6px"> {$joinResult[sec].itemName}</font></td>
                                <td align="right"><font  size="6px"> {$joinResult[sec].totalItem}</font></td>
								<td align="right"><font  size="6px"> {$joinResult[sec].itemPrice}</font></td>
								<td align="right"><font  size="6px"> {$joinResult[sec].totalItemAmount}</font></td>
                                
                            </tr>
                        {/section}
                    </tbody>
                    {if $userType == "admin"}
                        <tfoot>
                            <tr>
                                <th colspan="5" style="padding-right:5px;" style="text-align:left;" align="left">
                                    <select name="operation" style="visibility: hidden;">
                                        <option value="delete">delete</option>
                                    </select>
                                    <input type="submit" name="submit" value="DELETE" style="height: 30px; width: 100px">
                                    <label>
                                        <a class="big" href="deleteFromClose.php?trunk='true'">&nbsp&nbsp All Clear &nbsp&nbsp</a>
                                    </label>
                                </th>
                                <th><font size="5px" style="text-align: right">Total</font></th>
                                <th style="text-align: right"><font size="5px">{$grandTotal}</font></th>
                                <th style="text-align: right"><font size="5px">{$grandTotalItems}</font></th>
                                <th colspan="2">&nbsp;</th>
                            </tr>

                        </tfoot>
                    {/if}
                </table>
				
				
            </form>  
        </div>
    {/block}