# Database Backup#
# Backup Date: 2014-07-08 14:01:52

drop table if exists item;
create table item (
  itemId int(11) not null auto_increment,
  itemPrice int(11) not null ,
  itemName varchar(100) not null ,
  PRIMARY KEY (itemId)
);

insert into item (itemId, itemPrice, itemName) values ('1', '35', 'Vanila');
insert into item (itemId, itemPrice, itemName) values ('2', '40', 'Ice Cream');
insert into item (itemId, itemPrice, itemName) values ('3', '60', 'Shiv Special');
insert into item (itemId, itemPrice, itemName) values ('4', '30', 'King Cone');
insert into item (itemId, itemPrice, itemName) values ('5', '25', 'Candy');
insert into item (itemId, itemPrice, itemName) values ('6', '35', 'Sp. Mava Candy');
insert into item (itemId, itemPrice, itemName) values ('7', '100', 'Sundae');
insert into item (itemId, itemPrice, itemName) values ('8', '50', 'Juice');
insert into item (itemId, itemPrice, itemName) values ('9', '70', 'Faluda');
insert into item (itemId, itemPrice, itemName) values ('10', '45', 'Chickoo Pl');
insert into item (itemId, itemPrice, itemName) values ('11', '75', 'Vanila/Coffee Pl');
insert into item (itemId, itemPrice, itemName) values ('12', '95', 'Shake Pl');
insert into item (itemId, itemPrice, itemName) values ('13', '140', 'Shiv Sp. Pl');
insert into item (itemId, itemPrice, itemName) values ('14', '120', 'Noodles');
insert into item (itemId, itemPrice, itemName) values ('15', '140', 'Mush/Manch Noodles');
insert into item (itemId, itemPrice, itemName) values ('16', '120', 'Rice/Bhel');
insert into item (itemId, itemPrice, itemName) values ('17', '140', 'Pan/Mus Rice');
insert into item (itemId, itemPrice, itemName) values ('18', '120', 'M.Gravy/Dry');
insert into item (itemId, itemPrice, itemName) values ('19', '180', 'Pan.Ch.Gravy/Dry');
insert into item (itemId, itemPrice, itemName) values ('20', '190', 'Trippal Schezuan');
insert into item (itemId, itemPrice, itemName) values ('21', '150', 'Pizza');
insert into item (itemId, itemPrice, itemName) values ('22', '190', 'Shiv Sp. Pizaa');
insert into item (itemId, itemPrice, itemName) values ('23', '130', 'Club');
insert into item (itemId, itemPrice, itemName) values ('24', '90', 'Grill');
insert into item (itemId, itemPrice, itemName) values ('25', '85', 'Non Grill');
insert into item (itemId, itemPrice, itemName) values ('26', '130', 'Oven Toast');
insert into item (itemId, itemPrice, itemName) values ('27', '90', 'Garlic Bread');
insert into item (itemId, itemPrice, itemName) values ('29', '25', 'Cold Drink');
insert into item (itemId, itemPrice, itemName) values ('30', '20', 'Mineral Water');
insert into item (itemId, itemPrice, itemName) values ('31', '30', 'Extra Dry Fruits');
insert into item (itemId, itemPrice, itemName) values ('32', '40', 'Extra Topping');
insert into item (itemId, itemPrice, itemName) values ('33', '20', 'Manchurian Chopsuey');
insert into item (itemId, itemPrice, itemName) values ('34', '30', 'Extra Topping');
insert into item (itemId, itemPrice, itemName) values ('35', '10', 'Extra Chutney');
insert into item (itemId, itemPrice, itemName) values ('36', '50', 'Chickoo Spacial');
insert into item (itemId, itemPrice, itemName) values ('37', '80', 'Vanila/Coffee Spacial');
insert into item (itemId, itemPrice, itemName) values ('38', '100', 'Shake Sp');
insert into item (itemId, itemPrice, itemName) values ('39', '150', 'Shiv Sp. Sp.');
insert into item (itemId, itemPrice, itemName) values ('40', '85', 'Burger');
insert into item (itemId, itemPrice, itemName) values ('41', '80', 'French Fries');
insert into item (itemId, itemPrice, itemName) values ('42', '18', 'Ice Cream Half');
insert into item (itemId, itemPrice, itemName) values ('43', '20', 'Ice Cream Half');
insert into item (itemId, itemPrice, itemName) values ('44', '30', 'Ice Cream Half');
insert into item (itemId, itemPrice, itemName) values ('45', '350', 'Ice Cream');
insert into item (itemId, itemPrice, itemName) values ('46', '400', 'Ice Cream');
insert into item (itemId, itemPrice, itemName) values ('47', '600', 'Shiv Special IceCream');
insert into item (itemId, itemPrice, itemName) values ('48', '70', 'Bread Non Grill');
insert into item (itemId, itemPrice, itemName) values ('49', '75', 'Bread Grill');
drop table if exists ordereditems;
create table ordereditems (
  orderedItemsId int(11) not null auto_increment,
  orderId int(11) not null ,
  itemId int(11) not null ,
  quantity double default '0' not null ,
  weight int(11) default '0' not null ,
  orderTime time ,
  userName varchar(255) ,
  PRIMARY KEY (orderedItemsId)
);

insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('1', '1', '8', '1', '0', '13:06:59', 'om');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('2', '2', '10', '1', '0', '13:36:05', 'om');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('3', '3', '10', '2', '0', '13:41:20', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('4', '3', '8', '1', '0', '13:41:21', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('5', '3', '35', '1', '0', '13:46:52', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('6', '3', '35', '1', '0', '13:46:53', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('7', '3', '49', '1', '0', '13:50:48', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('8', '4', '35', '1', '0', '13:50:54', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('9', '4', '49', '1', '0', '13:50:57', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('10', '4', '47', '1', '250', '13:51:45', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('11', '4', '1', '1', '0', '13:53:46', 'omshiv');
insert into ordereditems (orderedItemsId, orderId, itemId, quantity, weight, orderTime, userName) values ('12', '4', '45', '1', '250', '13:53:56', 'omshiv');
drop table if exists ordermaster;
create table ordermaster (
  orderId int(11) not null auto_increment,
  waiterId int(11) not null ,
  tableId int(11) not null ,
  tablePart varchar(5) default 'A' not null ,
  startTime time not null ,
  startDate date ,
  orderstatus varchar(100) not null ,
  PRIMARY KEY (orderId)
);

insert into ordermaster (orderId, waiterId, tableId, tablePart, startTime, startDate, orderstatus) values ('1', '1', '1', 'A', '13:06:59', '2014-07-08', 'R');
insert into ordermaster (orderId, waiterId, tableId, tablePart, startTime, startDate, orderstatus) values ('2', '1', '1', 'A', '13:36:04', '2014-07-08', 'C');
insert into ordermaster (orderId, waiterId, tableId, tablePart, startTime, startDate, orderstatus) values ('3', '1', '1', 'A', '13:41:19', '2014-07-08', 'L');
insert into ordermaster (orderId, waiterId, tableId, tablePart, startTime, startDate, orderstatus) values ('4', '1', '2', 'A', '13:50:54', '2014-07-08', 'L');
drop table if exists user;
create table user (
  userId int(11) not null auto_increment,
  userName varchar(500) ,
  password varchar(50) ,
  userType varchar(255) not null ,
  PRIMARY KEY (userId)
);

insert into user (userId, userName, password, userType) values ('1', 'omshiv', 'shivom', 'admin');
insert into user (userId, userName, password, userType) values ('2', 'shiv1', 'shiv1', 'local');
insert into user (userId, userName, password, userType) values ('3', 'om', 'om', 'local');
drop table if exists waiter;
create table waiter (
  waiterId int(11) not null auto_increment,
  waiterName varchar(50) ,
  PRIMARY KEY (waiterId)
);

insert into waiter (waiterId, waiterName) values ('1', 'T');
insert into waiter (waiterId, waiterName) values ('2', 'N');
insert into waiter (waiterId, waiterName) values ('3', 'G');
insert into waiter (waiterId, waiterName) values ('4', 'A');
insert into waiter (waiterId, waiterName) values ('5', 'J');
insert into waiter (waiterId, waiterName) values ('6', 'D');
insert into waiter (waiterId, waiterName) values ('7', 'V');
insert into waiter (waiterId, waiterName) values ('8', 'B');
insert into waiter (waiterId, waiterName) values ('9', 'F');
insert into waiter (waiterId, waiterName) values ('10', 'O');
insert into waiter (waiterId, waiterName) values ('11', 'M');
insert into waiter (waiterId, waiterName) values ('12', 'L');
