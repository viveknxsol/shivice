<?php

include("include/config.inc.php");
if (!isset($_SESSION['s_activId'])) {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");
}
if (isset($_REQUEST['waiter'])) {
    $waiterVar = $_REQUEST['waiter'];
} else {
    $waiterVar = 0;
}

if (isset($_REQUEST['table'])) {
    $tableVar = $_REQUEST['table'];
} else {
    $tableVar = 0;
}
if (isset($_REQUEST['tablePart'])) {
    $tablePartVar = $_REQUEST['tablePart'];
} else {
    $tablePartVar = "A";
}

$orderstatus = "";
$joinResult = array();
$i = 0;
$allTablesSelected = isset($_POST['allTables']) ? $_POST['allTables'] : 'Closed Tables';
$joinOrderMasterOrderedItems = " SELECT waiter.waiterName, waiter.waiterId, ordermaster.tablePart,ordermaster.orderId,ordermaster.startDate,
                                        ordermaster.tableId, ordermaster.orderstatus,                                        
                                        COUNT( * ) AS totalItem, ordermaster.startTime, 
                                        MAX( ordereditems.orderTime ) AS lastordertime
                                   FROM ordermaster
                                   JOIN ordereditems ON ordermaster.orderId = ordereditems.orderId
                                   JOIN item ON item.itemId = ordereditems.itemId
                                   JOIN waiter ON waiter.waiterId = ordermaster.waiterId
                                  WHERE 1 = 1";
if (isset($_POST['allTables']) && $_POST['allTables'] == 'Closed Tables') {
    $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'C'";
} else if (isset($_POST['allTables']) && $_POST['allTables'] == 'Live Tables') {
    $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'L'";
} else if (isset($_POST['allTables']) && $_POST['allTables'] == 'Received Tables') {
    $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'R'";
}
if ($allTablesSelected == 'Closed Tables') {
    $joinOrderMasterOrderedItems .= " AND ordermaster.orderstatus = 'C'";
}
$joinOrderMasterOrderedItems .= " GROUP BY ordermaster.orderId ORDER BY ordermaster.startDate,ordermaster.startTime";

$joinOrderMasterOrderedItemsRes = mysql_query($joinOrderMasterOrderedItems) or die(mysql_error());
$grandTotal = 0;
$grandTotalItems = 0;
while ($joinOrderMasterOrderedItemsRow = mysql_fetch_array($joinOrderMasterOrderedItemsRes)) {
    $joinResult[$i]['orderId'] = $joinOrderMasterOrderedItemsRow['orderId'];
    $joinResult[$i]['waiterName'] = $joinOrderMasterOrderedItemsRow['waiterName'];
    $joinResult[$i]['waiterId'] = $joinOrderMasterOrderedItemsRow['waiterId'];
    $joinResult[$i]['tableId'] = $joinOrderMasterOrderedItemsRow['tableId'];
    $joinResult[$i]['tablePart'] = $joinOrderMasterOrderedItemsRow['tablePart'];
    $joinResult[$i]['totalamount'] = 0;
    $joinResult[$i]['totalItem'] = $joinOrderMasterOrderedItemsRow['totalItem'];
    $grandTotalItems+= $joinOrderMasterOrderedItemsRow['totalItem'];
    $joinResult[$i]['startTime'] = $joinOrderMasterOrderedItemsRow['startTime'];
    $joinResult[$i]['lastordertime'] = $joinOrderMasterOrderedItemsRow['lastordertime'];
    $joinResult[$i]['tablePart'] = $joinOrderMasterOrderedItemsRow['tablePart'];
    $joinResult[$i]['orderstatus'] = $joinOrderMasterOrderedItemsRow['orderstatus'];
    $forTotalAmount = " SELECT ordereditems.quantity , item.itemPrice,item.kgPrice, ordereditems.weight 
                                   FROM ordereditems
                                   JOIN item ON item.itemId = ordereditems.itemId
                                  WHERE ordereditems.orderId = " . $joinResult[$i]['orderId'];
    $forTotalAmountRes = mysql_query($forTotalAmount) or die(mysql_error());
    $forTotalAmountArray = array();
    $j = 0;
    while ($forTotalAmountRow = mysql_fetch_array($forTotalAmountRes)) {
        if ($forTotalAmountRow['weight'] == 0) {
            $forTotalAmountArray[$j]['amount'] = $forTotalAmountRow['itemPrice'] * $forTotalAmountRow['quantity'];
        } else {
            $forTotalAmountArray[$j]['amount'] = ceil((($forTotalAmountRow['kgPrice'] * $forTotalAmountRow['weight']) / 1000) * $forTotalAmountRow['quantity']);
        }
        $joinResult[$i]['totalamount'] += $forTotalAmountArray[$j]['amount'];
        $grandTotal += $forTotalAmountArray[$j]['amount'];
        $j++;
    }
    $i++;
}

//---------------- Item Wise grand Total  -----------------------------------------------------------------------------------
$todays = new DateTime();
$tod = $todays->format("d/m/Y");
if (isset($_POST["selDate"]) && $_POST["selDate"] !== "") {
    $dateWise = $_POST["selDate"];
} else {
    $dateWise = $tod;
}
$join2Result = array();
$l = 0;
$grandTotal2 = 0; //Item wise Grand total Amount
$grandTotalItems2 = 0; //Item wise Grand total no of orders
$sum = 0;
$selectWeight = "SELECT weight FROM ordereditems GROUP BY weight";
$selectWeightRes = mysql_query($selectWeight) or die(mysql_error());
while ($selectWeightRow = mysql_fetch_array($selectWeightRes)) {

    if ($selectWeightRow['weight'] == '0') {

        $joinItemOrderedItems = "SELECT DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y') as startDate, SUM(ordereditems.quantity) as totalItem2, ordereditems.itemId, item.itemName, item.itemPrice, ordereditems.weight, item.kgPrice,ordereditems.orderId
                        FROM `ordereditems` 
                        LEFT JOIN item ON ordereditems.itemId=item.itemId
                        LEFT JOIN ordermaster ON ordereditems.orderId=ordermaster.orderId
                        WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' AND ordereditems.weight=".$selectWeightRow['weight']."
                        GROUP BY ordereditems.itemId";

        $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
        while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
            $join2Result[$l]['totalItem2'] = $joinItemOrderedItemsRow['totalItem2'];
            $join2Result[$l]['itemId'] = $joinItemOrderedItemsRow['itemId'];
            $join2Result[$l]['weight'] = $joinItemOrderedItemsRow['weight'];
            $join2Result[$l]['itemName'] = $joinItemOrderedItemsRow['itemName'];
            $join2Result[$l]['itemPrice'] = $joinItemOrderedItemsRow['itemPrice'];
            $join2Result[$l]['orderId'] = $joinItemOrderedItemsRow['orderId'];
            $join2Result[$l]['totalItem2'] = intval($joinItemOrderedItemsRow['totalItem2']);
            $grandTotalItems2+= $join2Result[$l]['totalItem2'];
            $join2Result[$l]['TotalAmount'] = 0;

            $findItemIdTotalAmount = "SELECT item.itemId, ordereditems.quantity, ordereditems.weight, item.itemPrice, item.kgPrice 
                                    FROM ordereditems 
                                    LEFT JOIN item ON ordereditems.itemId=item.itemId                                                                         
                                    WHERE orderId IN (SELECT orderId FROM ordermaster WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' ) AND ordereditems.itemId=" . $joinItemOrderedItemsRow['itemId']." AND ordereditems.weight=".$selectWeightRow['weight'];
                                        
            $findItemIdTotalAmountRes = mysql_query($findItemIdTotalAmount) or die("Error 2: " . mysql_error());

            $rowTotalAmount = 0;
            while ($row = mysql_fetch_assoc($findItemIdTotalAmountRes)) {

                $join2Result[$l]['TotalAmount'] += $rowTotalAmount + ceil($row["quantity"] * $row["itemPrice"]);
            }
            $grandTotal2+=$join2Result[$l]['TotalAmount'];
            $l++;
        }
    } else if ($selectWeightRow['weight'] == '250') {

        $joinItemOrderedItems = "SELECT DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y') as startDate, SUM(ordereditems.quantity) as totalItem2, ordereditems.itemId, item.itemName, item.itemPrice, ordereditems.weight, item.kgPrice,ordereditems.orderId
                        FROM `ordereditems` 
                        LEFT JOIN item ON ordereditems.itemId=item.itemId
                        LEFT JOIN ordermaster ON ordereditems.orderId=ordermaster.orderId
                        WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' AND ordereditems.weight=".$selectWeightRow['weight']."
                        GROUP BY ordereditems.itemId";

        $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
        while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
            $join2Result[$l]['totalItem2'] = $joinItemOrderedItemsRow['totalItem2'];
            $join2Result[$l]['itemId'] = $joinItemOrderedItemsRow['itemId'];
            $join2Result[$l]['weight'] = '250 Gm';
            $join2Result[$l]['itemName'] = $joinItemOrderedItemsRow['itemName'];
            $join2Result[$l]['itemPrice'] = $joinItemOrderedItemsRow['kgPrice'];
            $join2Result[$l]['orderId'] = $joinItemOrderedItemsRow['orderId'];
            $join2Result[$l]['totalItem2'] = intval($joinItemOrderedItemsRow['totalItem2']);
            $grandTotalItems2+= $join2Result[$l]['totalItem2'];
            $join2Result[$l]['TotalAmount'] = 0;

            $findItemIdTotalAmount = "SELECT item.itemId, ordereditems.quantity, ordereditems.weight, item.itemPrice, item.kgPrice 
                                    FROM ordereditems 
                                    LEFT JOIN item 
                                    ON ordereditems.itemId=item.itemId 
                                    WHERE orderId IN (SELECT orderId FROM ordermaster WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' ) AND ordereditems.itemId=" . $joinItemOrderedItemsRow['itemId']." AND ordereditems.weight=".$selectWeightRow['weight'];
            $findItemIdTotalAmountRes = mysql_query($findItemIdTotalAmount) or die("Error 2: " . mysql_error());

            $rowTotalAmount = 0;
            while ($row = mysql_fetch_assoc($findItemIdTotalAmountRes)) {
              
                    $join2Result[$l]['TotalAmount'] += $rowTotalAmount + ceil($row["quantity"] * ($row["kgPrice"] / (1000 / $row["weight"])));
              
            }
            $grandTotal2+=$join2Result[$l]['TotalAmount'];
            $l++;
        }
    } else if ($selectWeightRow['weight'] == '500') {

        $joinItemOrderedItems = "SELECT DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y') as startDate, SUM(ordereditems.quantity) as totalItem2, ordereditems.itemId, item.itemName, item.itemPrice, ordereditems.weight, item.kgPrice,ordereditems.orderId
                        FROM `ordereditems` 
                        LEFT JOIN item ON ordereditems.itemId=item.itemId
                        LEFT JOIN ordermaster ON ordereditems.orderId=ordermaster.orderId
                        WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' AND ordereditems.weight=".$selectWeightRow['weight']."
                        GROUP BY ordereditems.itemId";

        $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
        while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
            $join2Result[$l]['totalItem2'] = $joinItemOrderedItemsRow['totalItem2'];
            $join2Result[$l]['itemId'] = $joinItemOrderedItemsRow['itemId'];
            $join2Result[$l]['weight'] = '500 Gm';
            $join2Result[$l]['itemName'] = $joinItemOrderedItemsRow['itemName'];
            $join2Result[$l]['itemPrice'] = $joinItemOrderedItemsRow['kgPrice'];
            $join2Result[$l]['orderId'] = $joinItemOrderedItemsRow['orderId'];
            $join2Result[$l]['totalItem2'] = intval($joinItemOrderedItemsRow['totalItem2']);
            $grandTotalItems2+= $join2Result[$l]['totalItem2'];
            $join2Result[$l]['TotalAmount'] = 0;

            $findItemIdTotalAmount = "SELECT item.itemId, ordereditems.quantity, ordereditems.weight, item.itemPrice, item.kgPrice 
                                    FROM ordereditems 
                                    LEFT JOIN item 
                                    ON ordereditems.itemId=item.itemId 
                                    WHERE orderId IN (SELECT orderId FROM ordermaster WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' ) AND ordereditems.itemId=" . $joinItemOrderedItemsRow['itemId']." AND ordereditems.weight=".$selectWeightRow['weight'];
            $findItemIdTotalAmountRes = mysql_query($findItemIdTotalAmount) or die("Error 2: " . mysql_error());

            $rowTotalAmount = 0;
            while ($row = mysql_fetch_assoc($findItemIdTotalAmountRes)) {
                    $join2Result[$l]['TotalAmount'] += $rowTotalAmount + ceil($row["quantity"] * ($row["kgPrice"] / (1000/($row["weight"]))));
            }
            $grandTotal2+=$join2Result[$l]['TotalAmount'];
            $l++;
        }
    } else if ($selectWeightRow['weight'] == '750') {

        $joinItemOrderedItems = "SELECT DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y') as startDate, SUM(ordereditems.quantity) as totalItem2, ordereditems.itemId, item.itemName, item.itemPrice, ordereditems.weight, item.kgPrice,ordereditems.orderId
                        FROM `ordereditems` 
                        LEFT JOIN item ON ordereditems.itemId=item.itemId
                        LEFT JOIN ordermaster ON ordereditems.orderId=ordermaster.orderId
                        WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' AND ordereditems.weight=".$selectWeightRow['weight']."
                        GROUP BY ordereditems.itemId";

        $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
        while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
            $join2Result[$l]['totalItem2'] = $joinItemOrderedItemsRow['totalItem2'];
            $join2Result[$l]['itemId'] = $joinItemOrderedItemsRow['itemId'];
            $join2Result[$l]['weight'] = '750 Gm';
            $join2Result[$l]['itemName'] = $joinItemOrderedItemsRow['itemName'];
            $join2Result[$l]['itemPrice'] = $joinItemOrderedItemsRow['kgPrice'];
            $join2Result[$l]['orderId'] = $joinItemOrderedItemsRow['orderId'];
            $join2Result[$l]['totalItem2'] = intval($joinItemOrderedItemsRow['totalItem2']);
            $grandTotalItems2+= $join2Result[$l]['totalItem2'];
            $join2Result[$l]['TotalAmount'] = 0;

            $findItemIdTotalAmount = "SELECT item.itemId, ordereditems.quantity, ordereditems.weight, item.itemPrice, item.kgPrice 
                                    FROM ordereditems 
                                    LEFT JOIN item 
                                    ON ordereditems.itemId=item.itemId 
                                    WHERE orderId IN (SELECT orderId FROM ordermaster WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' ) AND ordereditems.itemId=" . $joinItemOrderedItemsRow['itemId']." AND ordereditems.weight=".$selectWeightRow['weight'];
            $findItemIdTotalAmountRes = mysql_query($findItemIdTotalAmount) or die("Error 2: " . mysql_error());

            $rowTotalAmount = 0;
            while ($row = mysql_fetch_assoc($findItemIdTotalAmountRes)) {
                    $join2Result[$l]['TotalAmount'] += $rowTotalAmount + ceil($row["quantity"] * ($row["kgPrice"] / (1000 / $row["weight"])));
            }
            $grandTotal2+=$join2Result[$l]['TotalAmount'];
            $l++;
        }
    } else if ($selectWeightRow['weight'] == '1000') {

        $joinItemOrderedItems = "SELECT DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y') as startDate, SUM(ordereditems.quantity) as totalItem2, ordereditems.itemId, item.itemName, item.itemPrice, ordereditems.weight, item.kgPrice,ordereditems.orderId
                        FROM `ordereditems` 
                        LEFT JOIN item ON ordereditems.itemId=item.itemId
                        LEFT JOIN ordermaster ON ordereditems.orderId=ordermaster.orderId
                        WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' AND ordereditems.weight=".$selectWeightRow['weight']."
                        GROUP BY ordereditems.itemId";

        $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
        
        while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
            $join2Result[$l]['totalItem2'] = $joinItemOrderedItemsRow['totalItem2'];
            $join2Result[$l]['itemId'] = $joinItemOrderedItemsRow['itemId'];
            $join2Result[$l]['weight'] = '1 Kg';
            $join2Result[$l]['itemName'] = $joinItemOrderedItemsRow['itemName'];
            $join2Result[$l]['itemPrice'] = $joinItemOrderedItemsRow['kgPrice'];
            $join2Result[$l]['orderId'] = $joinItemOrderedItemsRow['orderId'];
            $join2Result[$l]['totalItem2'] = intval($joinItemOrderedItemsRow['totalItem2']);
            $grandTotalItems2+= $join2Result[$l]['totalItem2'];
            $join2Result[$l]['TotalAmount'] = 0;

            $findItemIdTotalAmount = "SELECT item.itemId, ordereditems.quantity, ordereditems.weight, item.itemPrice, item.kgPrice 
                                    FROM ordereditems 
                                    LEFT JOIN item 
                                    ON ordereditems.itemId=item.itemId 
                                    WHERE orderId IN (SELECT orderId FROM ordermaster WHERE DATE_FORMAT(ordermaster.startDate,'%d/%m/%Y')='" . $dateWise . "' ) AND ordereditems.itemId=" . $joinItemOrderedItemsRow['itemId']." AND ordereditems.weight=".$selectWeightRow['weight'];
            $findItemIdTotalAmountRes = mysql_query($findItemIdTotalAmount) or die("Error 2: " . mysql_error());
            
            while ($row = mysql_fetch_assoc($findItemIdTotalAmountRes)) {
                    $join2Result[$l]['TotalAmount'] += $rowTotalAmount + ceil($row["quantity"] * ($row["kgPrice"] / (1000 / $row["weight"])));
            }
            $grandTotal2+=$join2Result[$l]['TotalAmount'];
            $l++;
        }
    }
}

$tableStausValueArr[0] = "Closed Tables";
$tableStausOutputArr[0] = "Closed Tables";
$tableStausValueArr[1] = "Live Tables";
$tableStausOutputArr[1] = "Live Tables";
$tableStausValueArr[2] = "Received Tables";
$tableStausOutputArr[2] = "Received Tables";

include("./bottom.php");
$smarty->assign('todays', $dateWise);
$smarty->assign('tableVar', $tableVar);
$smarty->assign('sum', $sum);
$smarty->assign('grandTotal', $grandTotal);
$smarty->assign('grandTotalItems', $grandTotalItems);
$smarty->assign('grandTotal2', $grandTotal2);
$smarty->assign('grandTotalItems2', $grandTotalItems2);
$smarty->assign('tablePartVar', $tablePartVar);
$smarty->assign('waiterVar', $waiterVar);
$smarty->assign('joinResult', $joinResult);
$smarty->assign('join2Result', $join2Result);
$smarty->assign('allTablesSelected', $allTablesSelected);
$smarty->assign('tableStausValueArr', $tableStausValueArr);
$smarty->assign('tableStausOutputArr', $tableStausOutputArr);
$smarty->assign('orderstatus', $orderstatus);
$smarty->display('closeTables.tpl');
?>
