<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<html xmlns="http://www.w3.org/1999/xhtml">    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />    
    <head>
        <title>!! Shiv IceCream !!</title>
        <script type="text/javascript" src="./js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery.autocomplete.js"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="./css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="./css/menu.css" rel="stylesheet" type="text/css" />
        <link href="./css/waiterdiv.css" rel="stylesheet" type="text/css" />
        <link href="./css/jquery.autocomplete.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">

            #itemTable
            {
                border-color:black;	
            }
            .quntity
            {
                width:40px;
            }
            #iceCreams
            {
                font-size:15px;
                background:#FF6666;	
            }
            #ice
            {
                font-size:18px;
                background:#F8F8F8;
                color:red;	
            }
            #ice1
            {
                font-size:15px;
                background:#F8F8F8;	
            }
            .itemColor
            {
                color:black;
            }
            #waiter
            {
                font-size:20px;
            }
            #tableNo
            {
                font-size:20px;
            }
            .class
            {
                width:100px;	
            }
        </style>

        {block name=head}
            <script type="text/javascript">


                //SuckerTree Horizontal Menu (Sept 14th, 06)
                //By Dynamic Drive: http://www.dynamicdrive.com/style/
                var menuids = ["treemenu1"] //Enter id(s) of SuckerTree UL menus, separated by commas
                function buildsubmenus_horizontal()
                {
                    for (var i = 0; i < menuids.length; i++)
                    {
                        var ultags = document.getElementById(menuids[i]).getElementsByTagName("ul");
                        for (var t = 0; t < ultags.length; t++)
                        {
                            if (ultags[t].parentNode.parentNode.id == menuids[i])
                            {
                                //if this is a first level submenu
                                ultags[t].style.top = ultags[t].parentNode.offsetHeight + "px"; //dynamically position first level submenus to be height of main menu item
                                ultags[t].parentNode.getElementsByTagName("a")[0].className = "mainfoldericon";
                            }
                            else
                            {
                                //else if this is a sub level menu (ul)
                                ultags[t].style.left = ultags[t - 1].getElementsByTagName("a")[0].offsetWidth + "px"; //position menu to the right of menu item that activated it
                                ultags[t].parentNode.getElementsByTagName("a")[0].className = "subfoldericon";
                            }

                            ultags[t].parentNode.onmouseover = function ()
                            {
                                this.getElementsByTagName("ul")[0].style.visibility = "visible";
                            }
                            ultags[t].parentNode.onmouseout = function ()
                            {
                                this.getElementsByTagName("ul")[0].style.visibility = "hidden";
                            }
                        }
                    }
                }
                if (window.addEventListener)
                {
                    window.addEventListener("load", buildsubmenus_horizontal, false);
                }
                else if (window.attachEvent)
                {
                    window.attachEvent("onload", buildsubmenus_horizontal);
                }

                $(function () {
                    $('.threeToFixed').blur(function () {
                        applyToFixed($(this));
                    });
                });

                function applyToFixed(obj)
                {
                    var objValueToFixed = parseFloat($(obj).val());
                    $(obj).val(objValueToFixed.toFixed(2));
                }
                $("#juicePlus").on('click', function () {
                    $("#juiceQty").val(parseInt($("#juiceQty").val()) + 1);
                });
                $("#juiceMinus").on('click', function () {
                    $("#juiceQty").val(parseInt($("#juiceQty").val()) - 1);
                });
                `   $("#faludaPlus").on('click', function () {
                    $("#faludaQty").val(parseInt($("#faludaQty").val()) + 1);
                });
                $("#faludaMinus").on('click', function () {
                    $("#faludaQty").val(parseInt($("#faludaQty").val()) - 1);
                });


            </script>
        {/block}
    </head>
    
    <body>
        
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content">
                    <font size="4px">
                        <a href="logout.php" class="link">Logout</a> |
                        <a href="index.php?waiter={$waiterVar}&table={$tableVar}&tablePart={$tablePartVar}">Home</a> |
                        <a href="closeTables.php"> Closed Tables </a>	      
                        {if $s_userType=="admin"}
                            | <a href="items.php">Items</a>
                        {/if}
                    </font>
                </td>
            </tr>
            {if $s_userType=="admin"}
                <tr align="center">
                    <td class="bluebg1">
                        <div class="suckertreemenu">
                            <br />
                            <ul id="treemenu1">
                                <li><a href="backup.php" style="width:15px;" class="link">&#2384;</a></li>
                            </ul>
                            <br /><br /><br />
                        </div>
                    </td>
                </tr>
            {/if}
            <tr>
                <td class="content">                    
                {block name=body}{/block}
            </td>
        </tr>
        <tr>
            <td class="bluebg1">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td><img src="images/corner3.gif" width="6" height="7" /></td>
                        <td></td>
                        <td align="right"><img src="images/corner4.gif" width="6" height="7" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>