-- phpMyAdmin SQL Dump
-- version 3.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 19, 2013 at 07:43 AM
-- Server version: 5.0.67
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shiv`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` int(11) NOT NULL auto_increment,
  `itemPrice` int(11) NOT NULL,
  `itemName` varchar(100) NOT NULL,
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `itemPrice`, `itemName`) VALUES
(1, 35, 'Ice Cream'),
(2, 40, 'Ice Cream'),
(3, 60, 'Shiv Special'),
(4, 30, 'King Cone'),
(5, 25, 'Candy'),
(6, 35, 'Sp. Mava Candy'),
(7, 100, 'Sunday'),
(8, 50, 'Juice'),
(9, 70, 'Faluda'),
(10, 45, 'Chickoo Pl'),
(11, 75, 'Vanila/Coffee Pl'),
(12, 95, 'Shake Pl'),
(13, 140, 'shiv Sp. pl'),
(14, 120, 'Noodles'),
(15, 140, 'Mush/Manch Noodles'),
(16, 120, 'Rice/Bhel'),
(17, 140, 'Pan/Mus Rice'),
(18, 120, 'M.Gravy/Dry'),
(19, 180, 'Pan.Ch.Gravy/Dry'),
(20, 190, 'Trippal Schezuan'),
(21, 150, 'Pizza'),
(22, 180, 'Shiv Special Pizaa'),
(23, 130, 'Club'),
(24, 90, 'Grill'),
(25, 85, 'Non Grill'),
(26, 130, 'Oven Tost'),
(27, 80, 'Garlic Bread'),
(29, 25, 'Cold Drink'),
(30, 20, 'Mineral Water'),
(31, 30, 'Extra Dry Fruits'),
(32, 40, 'Mashroom Paneer'),
(33, 20, 'Manchurian Chopsuey'),
(34, 30, 'Extra Per Toppings'),
(35, 10, 'Extra Chutney'),
(36, 50, 'Chickoo Spacial'),
(37, 80, 'Vanila/Coffee Spacial'),
(38, 100, 'Shake Sp'),
(39, 150, 'shiv Sp. Sp.'),
(40, 85, 'Burger'),
(41, 80, 'French Fries'),
(42, 18, 'Ice Cream Half'),
(43, 20, 'Ice Cream Half'),
(44, 30, 'Ice Cream Half'),
(45, 350, 'Ice Cream'),
(46, 400, 'Ice Cream'),
(47, 600, 'Shiv Special IceCream'),
(48, 70, 'Bread Non Grill'),
(49, 75, 'Bread Grill');

-- --------------------------------------------------------

--
-- Table structure for table `ordereditems`
--

CREATE TABLE IF NOT EXISTS `ordereditems` (
  `orderedItemsId` int(11) NOT NULL auto_increment,
  `orderId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `quantity` double NOT NULL default '0',
  `weight` int(11) NOT NULL default '0',
  `orderTime` time default NULL,
  `userName` varchar(255) default NULL,
  PRIMARY KEY  (`orderedItemsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ordereditems`
--


-- --------------------------------------------------------

--
-- Table structure for table `ordermaster`
--

CREATE TABLE IF NOT EXISTS `ordermaster` (
  `orderId` int(11) NOT NULL auto_increment,
  `waiterId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  `tablePart` varchar(5) NOT NULL default 'A',
  `startTime` time NOT NULL,
  `startDate` date default NULL,
  `orderstatus` varchar(100) NOT NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ordermaster`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL auto_increment,
  `userName` varchar(500) default NULL,
  `password` varchar(50) default NULL,
  `userType` varchar(255) NOT NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `userName`, `password`, `userType`) VALUES
(1, 'om', 'om', 'admin'),
(2, 'om1', 'om1', 'local');

-- --------------------------------------------------------

--
-- Table structure for table `waiter`
--

CREATE TABLE IF NOT EXISTS `waiter` (
  `waiterId` int(11) NOT NULL auto_increment,
  `waiterName` varchar(50) default NULL,
  PRIMARY KEY  (`waiterId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `waiter`
--

INSERT INTO `waiter` (`waiterId`, `waiterName`) VALUES
(1, 'T'),
(2, 'N'),
(3, 'G'),
(4, 'A'),
(5, 'J'),
(6, 'D'),
(7, 'V'),
(8, 'B'),
(9, 'F'),
(10, 'O'),
(11, 'M'),
(12, 'L');
