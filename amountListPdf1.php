<?php

define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include("include/config.inc.php");

$orderId = isset($_REQUEST['orderId']) ? $_REQUEST['orderId'] : 0;
$totalAmount = 0;
$totalQuantity = 0;
$orderArray = array();
$selectOrder = "SELECT item.itemName,orderedItems.quantity,orderTime,ordereditems.orderId,ordereditems.parcel,ordermaster.startTime,
                            waiterName,ordermaster.tableId,ordereditems.itemId,waiter.waiterId,ordermaster.tableId,ordereditems.weight,
                            item.itemPrice,item.kgPrice,DATE_FORMAT(ordermaster.startDate,'%d-%m-%y') AS startDate
                  FROM ordereditems 
                  LEFT JOIN ordermaster ON ordereditems.orderId = ordermaster.orderId
                  LEFT JOIN item ON ordereditems.itemId = item.itemId
                  LEFT JOIN waiter ON ordermaster.waiterId = waiter.waiterId
                 WHERE ordermaster.orderId = " . $orderId. " ORDER BY ordereditems.parcel ASC";
        
$selectOrderRes = mysql_query($selectOrder);
$numRows = mysql_num_rows($selectOrderRes);

$minHeight = 76.2;
$height = 85;
$rowHeight = 5;

$addRowHeight = $rowHeight * $numRows;
$setHeight = $height + $addRowHeight;

if ($minHeight > $setHeight) {
    $setHeight = $minHeight;
}


$pdf = new FPDF('P', 'mm', array(78.5, $setHeight));   //Create new pdf file 76.2 width
$pdf->Open();     //Open file
$pdf->SetAutoPageBreak(false);  //Disable automatic page break
$pdf->AddFont('estre','','estre.php'); //STRANGELO EDESSA
$pdf->AddFont('vrinda','','ebrima.php'); 
$pdf->AddFont('ebrimabd','','ebrimabd.php'); 
$pdf->AddFont('vrinda','','vrinda.php'); 
$pdf->AddFont('vrindab','','vrindab.php'); 
$pdf->AddPage();  //Add first page

$i = 0;
$yAxis = 19;
$yAxis = $yAxis + $rowHeight;
pageHeader();
while ($orderRow = mysql_fetch_array($selectOrderRes)) {
    $itemName = $orderRow['itemName'];
    if ($orderRow['weight'] == 1000) {
        $itemName = $itemName ." ". $orderRow['quantity'] . "kg";
    } else if (($orderRow['weight'] < 1000) && ( $orderRow['weight'] > 0)) {
        $itemName = $itemName ." ". $orderRow['weight'] . "gm";
    } else if ($orderRow['weight'] == 0) {
        $itemName = $itemName;
    }
    $kgPrice = $orderRow['kgPrice'];    
    $quantity = $orderRow['quantity'];
    $waiterName = $orderRow['waiterName'];
    $startDate = $orderRow['startDate'];
    $startTime = $orderRow['startTime'];
    $tableId = $orderRow['tableId'];
    if ($orderRow['weight'] == 0) {
        $amount = $orderRow['itemPrice'] * $orderRow['quantity'];
        $itemPrice = $orderRow['itemPrice'];        
    } else {
        $amount = ceil((($orderRow['kgPrice'] * $orderRow['weight']) / 1000) * $orderRow['quantity']);
            $itemPrice = $orderRow['kgPrice'];
    }
    $totalQuantity += $orderRow['quantity'];
    $totalAmount += $amount;

    $i++;
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetXY(5, $yAxis + 18);
    $pdf->SetFont('vrinda','', 10);
    $pdf->Cell(36, 5, $itemName, 0, 0, 'L');
    $pdf->SetFont('vrinda','', 10); 
    if ($orderRow['parcel'] == 'p') {
        $pdf->Image('./images/parcel.png', $pdf->GetX(),$pdf->GetY(),4,5);
        $pdf->Cell(11, 5, $quantity, 0, 0, 'R');
    } else {
        $pdf->Cell(11, 5, $quantity, 0, 0, 'R');
    }
    $pdf->Cell(10, 5, $itemPrice, 0, 0, 'R');
    $pdf->Cell(12, 5, $amount, 0, 0, 'R');
    $yAxis += $rowHeight;
    $i++;
}

$updateOrdredItems = "UPDATE ordermaster 
                         SET orderstatus = 'C'
                       WHERE orderId = " . $orderId;
$updateOrdredItemsRes = mysql_query($updateOrdredItems);

$pdf->SetTextColor(0, 0, 0);
//$pdf->SetFont('Arial', 'B', 15);
$pdf->SetFont('vrindab','', 15);
$pdf->SetXY(40, 25);
$pdf->Cell(17.5, 10, $waiterName, 1, 0, 'C');
$pdf->SetXY(57.5, 25);
$pdf->Cell(17.6, 10, $tableId, 1, 0, 'C');
//$pdf->SetFont('Arial', 'B', 7);
$pdf->SetFont('vrindab','', 7);
$pdf->SetXY(17, 25);
$pdf->Cell(23, 10, $startDate, 0, 0, 'L');
$pdf->SetTextColor(0, 0, 0);
$pdf->SetFont('vrinda','', 11);
$pdf->SetXY(3, $yAxis + 20);
$pdf->Line(44, $yAxis + 20, 53, $yAxis + 20);
$pdf->Line(64, $yAxis + 20, 73, $yAxis + 20);
//$pdf->SetFont('Arial', 'B', 12);
$pdf->SetFont('vrindab','', 12);
$pdf->Cell(36, 5, 'Total', 0, 0, 'R', 0);
$pdf->Cell(13, 7, $totalQuantity, 0, 0, 'R', 0);
//$pdf->SetFont('Arial', 'B', 17);
$pdf->SetFont('vrindab','', 17);
$pdf->Cell(10.5, 5, '', 0, 0, 'C', 0);
$pdf->Cell(12, 7, $totalAmount, 0, 0, 'R', 0);
$pdf->SetXY(5, 3);
$pdf->Cell(70.2, $yAxis + 55, '', 1, 0, 'C'); 
//Set Row Height
//Create file
$pdf->Output();

//header part start
function pageHeader() {
    global $pdf;
       
    $pdf->Image('./images/shivlogo.jpg', 5, 3, 70.2, 16);
    //$pdf->SetFont('Arial', 'B', 8);
    $pdf->SetFont('vrindab','', 8);
    $pdf->SetXY(5, 19);
    $pdf->Cell(70.2, 3, 'Race Course Ring Road, Rajkot-1.', 0, 0, 'C');        
    $pdf->SetFont('vrinda','', 10);
    $pdf->SetXY(5, 25);
    $pdf->Cell(35, 10, 'Date:', 1, 0, 'L');
    $pdf->SetTextColor(0, 0, 0);    
    //$pdf->SetFont('Arial', 'B', 10);
    $pdf->SetFont('vrindab','', 10);
    $pdf->SetXY(5, 37);
    $pdf->Cell(36, 5, 'Item', 0, 0, 'C', 0);
    $pdf->Cell(11, 5, 'Qty', 0, 0, 'R', 0);
    $pdf->Cell(10.5, 5, 'Rate', 0, 0, 'R', 0);
    $pdf->Cell(12.6, 5, 'Amt', 0, 0, 'C', 0);
    $pdf->Line(19, 42, 28, 42);
    $pdf->Line(44, 42, 53, 42);
    $pdf->Line(55, 42, 62, 42);
    $pdf->Line(65, 42, 73, 42);
}

?>
