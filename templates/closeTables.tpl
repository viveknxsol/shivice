{extends file="main.tpl"}
{block name=head}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="./media/js/jquery.js" type="text/javascript"></script>
    <script src="./media/js/jquery.dataTables.js" type="text/javascript"></script>     

    <script>
        function printFun(orderIdVar)
        {
            window.open('amountListPdf1.php?orderId =' + orderIdVar);
        }
    </script>
    <style type="text/css">
        @import "./media/css/demo_table_jui.css";
        @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
    </style>

    <style>
        *{
            font-family: arial;
        }

        .big {
            margin-left:20px;
            border: 1px solid black;
            width:100px;
            height:100px;
            font-size:20px;
            background-color:#F3F3EE;
            cursor:pointer;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#datatables').dataTable({
                "iDisplayLength":25,
                "sPaginationType": "full_numbers",
                "aaSorting": [[2, "desc"]],
                "bJQueryUI": true
            });
        })

        function checkAll(status)
        {
            var checkbox = document.getElementsByName('CloseTableChk[]');
            for (i = 0; i < checkbox.length; i++)
            {
                checkbox[i].checked = status;
            }
        }

        function trunkFun()
        {
            window.parent.location.href = "deleteFromClose.php?trunk='true'";
        }
        function popupView(orderId)
        {
            window.open("viewTable.php?orderId=" + orderId, "", "width=700, height=500");
            return false;
        }
    </script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#datatables2').dataTable({
                "iDisplayLength": 100,
                "sPaginationType": "full_numbers",
                "aaSorting": [[2, "desc"]],
                "bJQueryUI": true,
				"aoColumnDefs": [{ "bVisible": true, "aTargets": [0] }]
            });
        })

        function checkAll(status)
        {
            var checkbox = document.getElementsByName('CloseTableChk[]');
            for (i = 0; i < checkbox.length; i++)
            {
                checkbox[i].checked = status;
            }
        }

        function trunkFun()
        {
            window.parent.location.href = "deleteFromClose.php?trunk='true'";
        }
    </script>
{/block}
{block name=body}
    <form name="tables" method="post" action="{$smarty.server.PHP_SELF}">
        <table align="center" style="background-color:#F7B64A; box-shadow: 10px 10px 5px #888888; height: 5em;width: 20em; -moz-border-radius: 1em 4em 1em 4em; border-radius: 1em 4em 1em 4em;"  border="0" cellpadding="2" cellspacing="2" id="tableNo" width="50%" class="waiterdiv" >
            <tr>
                <td align="center" valign="top"><font size="7px">Order Master</b></td>
            </tr>
            <tr>
                <td align="center">
                    <select name="allTables" id="allTables" autofocus="autofocus" onchange="this.form.submit();">
                        <option value="0">All Table</option>
                        {html_options values=$tableStausValueArr output=$tableStausOutputArr selected=$allTablesSelected}
                    </select>
                </td>
            </tr>
        </table>
    </form>
    <div><br />
        <form method="post" action="deleteFromClose.php">
            <table id="datatables" class="display">
                </tfoot>	  
                <thead>
                    <tr>
                        {if $s_userType == "admin"}
                            <!--<th><font size="5px">O.ID</font></th>-->
                            <th><input type="checkbox" name="CloseTableChkAll" onclick="checkAll(this.checked);"></th>
                            <th><font size="4px">Payment</font></th>
                            {/if}
                        <th><font size="5px">Print</font></th>
                        <th><font size="5px">Table</font></th>
                        <th><font size="5px">Waiter</font></th>
                        <th><font size="5px">Table Part</font></th>
                        <th><font size="5px">Total Amount</font></th>
                        <th><font size="5px">Total Items</font></th>
                        <th><font size="5px">Start Time</font></th>
                        <th><font size="5px">Last Order Time</font></th>
                            {if $s_userType == "admin"}
                            <th><font size="5px">View Details</font></th>
                            {/if}
                    </tr>
                </thead>
                <tbody>
                    {section name="sec" loop=$joinResult} 
                        <tr class="{cycle values="odd,even"}">
                            {if $s_userType == "admin"}
                                <!--<td>{$joinResult[sec].orderId}</td>-->
                                <td><input type="checkbox" name="CloseTableChk[]" value="{$joinResult[sec].orderId}"/></td>	
                                <td><a href="receive.php?orderId={$joinResult[sec].orderId}" name="rec">
                                        {if {$joinResult[sec].orderstatus} eq 'R'}<font size="6px" color="green">Received</font></a>
                                        {else if {$joinResult[sec].orderstatus} eq 'L'}<font size="6px" color="green">Live</font>{else}<font size="6px" color="green">Closed</font>{/if}
                                    </td>
                                {/if}
                                <td><a href="amountListPdf1.php?orderId={$joinResult[sec].orderId}"><font size="6px" color="green">Print</font></a></td>
                                <td align="right" style="color:red; font-weight: bold; font-size: 30px;">{$joinResult[sec].tableId}</td>
                                <td align="center" style="font-weight: bold;font-size: 30px;">{$joinResult[sec].waiterName}</td>
                                <td align="center" style=" font-weight: bold; font-size: 30px;">{$joinResult[sec].tablePart}</td>
                                <td align="right" style="color:olive; font-size: 30px;">{$joinResult[sec].totalamount}</td>
                                <td align="right" style="color:red; font-weight: bold; font-size: 30px;">{$joinResult[sec].totalItem}</td>
                                <td align="center" style="font-weight: bold; font-size: 30px;">{$joinResult[sec].startTime}</td>
                                <td align="center" style="color:red; font-weight: bold; font-size: 30px;">{$joinResult[sec].lastordertime}</td>
                                    {if $s_userType == "admin"}
                                    <td align="center"><font size="6px"><button id="{$joinResult[sec].orderId}" onclick="return popupView(this.id)" Value="View">View</button></font></td>
                                    {/if}
                            </tr>
                        {/section}
                    </tbody>
                    {if $s_userType == "admin"}
                        <tfoot>
                            <tr>
                                <th colspan="4" style="padding-right:5px;" style="text-align:left;" align="left">
                                    <select name="operation" style="visibility: hidden;">
                                        <option value="delete">delete</option>
                                    </select>
                                    <input type="submit" name="submit" onclick="return confirm('Are sure to Delete?')" value="DELETE" style="height: 30px; width: 100px">
                                    <label onclick="return confirm('Are sure to Clear All Record?')">
                                        <a class="big" href="deleteFromClose.php?trunk='true'">&nbsp&nbsp All Clear &nbsp&nbsp</a>
                                    </label>
                                </th>
                                <th><font size="5px" style="text-align: right">Total</font></th>
                                <th></th>
                                <th style="text-align: right"><font size="5px">{$grandTotal}</font></th>
                                <th style="text-align: right"><font size="5px">{$grandTotalItems}</font></th>
                                <th colspan="3">&nbsp;</th>
                            </tr>

                        </tfoot>
                    {/if}
                </table>
            </form>  
            <hr>
            {if $s_userType == "admin"}
                <table align="center"><tr align="center"><td><h1>Item Wise Details</h1></td><td>
                            <script src="./js/datepicker/jquery-ui.js"></script>
                            <script>
                                var tod=new Date();
                                        $(function () {
                                            $("#datepicker").datepicker({
                                                dateFormat: "dd/mm/yy",
                                                
                                            }).val("{$todays}");
                                            //$("#datepicker").datepicker("setDate",new Date());
                                        });
                                        
                            </script>
                            <form method="post" action="closeTables.php?">
                                Date: <input type="text" id="datepicker" name="selDate" >
                                <input type="submit" name="datebutton" value="Go">
								<input type="hidden" id="allTablesHidden" name="allTables" value="">
								<script> $('#allTablesHidden').attr('value',$('#allTables').val());</script>								
                            </form>
                        </td>
                    </tr>
                </table>
                <table id="datatables2" class="display">
                    <thead>
                        <tr>
                            <th><font size="5px">Item Id</font></th>
                            <th><font size="5px">Item Name</font></th>
                            <th><font size="5px">Quantity</font></th>
                            <th><font size="5px">Weight</font></th>
                            <th><font size="5px">Price (per Item)</font></th>
                            <th><font size="5px">Total Billed Amount</font></th>
                        </tr>
                    </thead>
                    <tbody>
                        {section name="sec" loop=$join2Result} 
                            <tr class="{cycle values="odd,even"}">
                                <td align="right" style="color:red; font-weight: bold; font-size: 30px;">{$join2Result[sec].itemId}</td>
                                <td align="left" style="font-weight: bold; font-size: 30px;">{$join2Result[sec].itemName}</td>
                                <td align="right" style="color:olive; font-size: 30px;">{$join2Result[sec].totalItem2}</td>
                                <td align="right" style="color:olive; font-size: 30px;">{$join2Result[sec].weight}</td>
                                <td align="right" style="color:olive; font-size: 30px;">{$join2Result[sec].itemPrice}</td>
                                <td align="right" style="color:red; font-size: 30px;">{$join2Result[sec].TotalAmount}</td>
                            </tr>
                        {/section}
                    </tbody>
                    {if $s_userType == "admin"}
                        <tfoot>
                            <tr>
                                <th></th>
                                <th><font size="5px" style="text-align: right">Total</font></th>
                                <th style="text-align: right"><font size="5px">{$grandTotalItems2}</font></th>
                                <th></th><th></th>
                                <th style="text-align: right"><font size="5px">{$grandTotal2}</font></th>

                            </tr>
                        </tfoot>
                    {/if}
                </table>
            {/if}                
        </div>
    {/block}