<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');

$pdf = new FPDF("P", "mm", array(76.2,76.2)); 
$pdf->addPage();

$pdf -> w; // Width of Current Page
$pdf -> h; // Height of Current Page

$pdf -> Line(0, 0, $pdf -> w, $pdf -> h);
$pdf -> Line($pdf -> w, 0, 0, $pdf -> h);

$pdf->Output('mypdf.pdf', 'I'); 
?>