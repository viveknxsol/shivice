{extends file="main.tpl"}
{block name=head}
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script src="./media/js/jquery.js" type="text/javascript"></script>
    <script src="./media/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="./media/js/jscolor/jscolor.js" type="text/javascript"></script>
    <style type="text/css">
        @import "./media/css/demo_table_jui.css";
        @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
    </style>

    <style>
        *{
            font-family: arial;
        }
        .big {
            margin-left:20px;
            border: 1px solid black;
            width:100px;
            height:100px;
            font-size:20px;
            background-color:#F3F3EE;
            cursor:pointer;
        }
    </style>


    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {
            $('#datatables').dataTable({
                "iDisplayLength": 100,
                "sPaginationType": "full_numbers",
                "aaSorting": [[3, "asc"]],
                "bJQueryUI": true,
                "aoColumnDefs": [{ "bVisible": true, "aTargets": [0] }]
            });
        });
    </script>

    <script>
        function confirmDelete()
        {
            return confirm("Are you sure to DELETE this item?");
        }
        function confirmEdit()
        {
            $('#itemPrice').css('background-color','white');
            $('#kgPrice').css('background-color','white');
            $('#itemSeq').css('background-color','white');
            
            if ($('#itemPrice').val() < 0) {
                $('#itemPrice').css('background-color','red');                
                return false;
            }
            if ($('#kgPrice').val() < 0) {
                $('#kgPrice').css('background-color','red');
                return false;
            }
            if (($('#itemSeq').val() <= 0) && $('#itemSeq').val()!="") {
                $('#itemSeq').css('background-color','red');
                return false;
            }           
            
            
            if (document.getElementById("submit").value == "Edit") {
                return confirm("Are you sure to EDIT this item");
                }
            else {
                    return confirm("Are you sure to ADD this item");
                }
        }
        function confirmReset()
        {
            document.getElementById("itemId").value = "0";
            document.getElementById("editPhoto").value = "";
            document.getElementById("itemName").value = "";
            document.getElementById("itemPrice").value = "0";
            document.getElementById("kgPrice").value = "0";
            document.getElementById("itemSeq").value = "";
            document.getElementById("photo").value = "";            
            document.getElementById("submit").value = "Add";            
        }
    </script>

    <style type="text/css">
        .itemTd{
            border: 1px solid #cccccc;
        }
    </style>
{/block}

{block name=body}    
    <form method="POST" id="myForm" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data" name="itemsForm">
        <center>
            <table class="itemTd" cellspacing="0" cellpadding="5px" width="500px">
                <caption>Items</caption>
                <input type="hidden" id="itemId" name="itemId" value="{$EdititemsArray.itemId}">
                <input type="hidden" id="editPhoto" name="editPhoto" value="{$EdititemsArray.itemPhoto}">
                <tr><td class="itemTd">Item Name:</td><td class="itemTd"><input required type="text" id="itemName" name="itemName" value="{$EdititemsArray.itemName}"> </td></tr>
                <tr><td class="itemTd">Unit Price:</td><td class="itemTd"><input required type="number" id="itemPrice" name="itemPrice" value="{$EdititemsArray.itemPrice}">Rs.</td></tr>
                <tr><td class="itemTd">1 Kg Price:</td><td class="itemTd"><input required type="number" id="kgPrice" name="kgPrice" value="{$EdititemsArray.kgPrice}">Rs.</td></tr>
                <tr><td class="itemTd">Price Type:</td><td class="itemTd"> <select name="callJs" >
                            {if $EdititemsArray.callJs=="setIceCreamItemId"}
                                <option value="setIceCreamItemId" selected>Unit and 1 Kg</option>
                                <option value="setItemId">Only Unit</option>
                            {else}
                                <option value="setIceCreamItemId">Unit and 1 Kg</option>
                                <option value="setItemId" selected>Only Unit</option>
                            {/if}
                        </select>
                    </td>
                </tr>
                <tr><td class="itemTd">Display Sequence:</td> <td class="itemTd"><input type="number" id="itemSeq" name="itemSeq" value="{$EdititemsArray.itemSeq}"></td></tr>
                <tr><td class="itemTd">Item Photo:</td><td class="itemTd"><input type="file" id="photo" name="photo"></td></tr>                
                <tr>
                    <td class="itemTd">Background Color:</td><td class="itemTd"><input type="text" name="backColor"  class="color" value="{$EdititemsArray.backColor}"></td>
                </tr>

                <tr><td class="itemTd">Visible:</td><td class="itemTd"> <select name="visibled">
                            {if $EdititemsArray.visibled=="1"}
                                <option value="0" >Hiden</option>
                                <option value="1" Selected>Visible</option>
                            {else}
                                <option value="0" Selected>Hiden</option>
                                <option value="1" >Visible</option>
                            {/if}
                        </select> </td></tr>
                <tr>
                    <td colspan="2" align="center" class="itemTd">
                        <input type="submit" id="submit" name="submit" value="{$EdititemsArray.submitvalue}" onclick="return confirmEdit(this);" >&nbsp;
                        <input type="button" name="Reset" value="Reset" onclick="confirmReset();" >
                    </td>
                </tr>
            </table><br>
        </center>
        <table border="0" id="datatables" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>id</th>                        
                    <th>Edit</th>                        
                    <th>Delete</th>
                    <th>Squence</th>
                    <th>Price type</th>
                    <th>Item Name</th>
                    <th>Unit Price</th>
                    <th>Kg Price</th>
                    <th>Photo</th>
                    <th>Visible/Hidden</th>                                
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>id</th>                                            
                    <th>Edit</th>                        
                    <th>Delete</th>
                    <th>Squence</th>
                    <th>Price type</th>
                    <th>Item Name</th>
                    <th>Unit Price</th>
                    <th>Kg Price</th>
                    <th>Photo</th>
                    <th>Visible/Hidden</th>                                
                </tr>                
            </tfoot>
            <tbody>
                {section name="itemsec" loop=$itemsArray}
                    <tr>
                        <td>{$itemsArray[itemsec].itemId}</td>
                        <td><a href="items.php?do=edit&itemid={$itemsArray[itemsec].itemId}" > Edit</a></td>
                        <td><a href="deleteItems.php?do=del&itemid={$itemsArray[itemsec].itemId}" onclick="return confirmDelete();" > Delete</a></td>
                        <td>{$itemsArray[itemsec].itemSeq}</td>
                        <td>{$itemsArray[itemsec].callJs}</td>
                        <td>{$itemsArray[itemsec].itemName}</td>
                        <td>{$itemsArray[itemsec].itemPrice}</td>
                        <td>{$itemsArray[itemsec].kgPrice}</td>
                        <td ><div style="background-color:{$itemsArray[itemsec].backColor}; padding: 1em;   border: 1px solid black;">
                                <img src="{$itemsArray[itemsec].itemPhoto}" width="45" height="45"> &nbsp{$itemsArray[itemsec].backColor} </div></td>
                        <td>{$itemsArray[itemsec].visibled}</td>
                    </tr>
                {/section}

            </tbody>

        </table>

    </form>  
{/block}
