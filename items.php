<?php

//echo "hello";
include("include/config.inc.php");
if (!isset($_SESSION['s_activId'])) {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");
}
if (isset($_REQUEST['waiter'])) {
    $waiterVar = $_REQUEST['waiter'];
} else {
    $waiterVar = 0;
}
$smarty->assign('waiterVar', $waiterVar);

if (isset($_REQUEST['table'])) {
    $tableVar = $_REQUEST['table'];
} else {
    $tableVar = 0;
}
$smarty->assign('tableVar', $tableVar);

if (isset($_REQUEST['tablePart'])) {
    $tablePartVar = $_REQUEST['tablePart'];
} else {
    $tablePartVar = "A";
}
$smarty->assign('tablePartVar', $tablePartVar);


$i = 0;
$itemsArray = array();
$EdititemsArray = array();

$EdititemsArray['itemId'] = 0;
$EdititemsArray['itemName'] = "";
$EdititemsArray['itemPrice'] = "0";
$EdititemsArray['kgPrice'] = "0";
$EdititemsArray['itemSeq'] = "";
$EdititemsArray['itemPhoto'] = "";
$EdititemsArray['callJs'] = "";
$EdititemsArray['visibled'] = "1";
$EdititemsArray['submitvalue'] = "Add";
$EdititemsArray['backColor'] = "FFFFFF";


if (isset($_POST["submit"])) {
$cmd = "SELECT Max(itemSeq) AS maxSeq FROM item";
        $resultmax = mysql_query($cmd) or die("Max(itemSeq)  Error: " . mysql_error());
        $newSeq = mysql_fetch_array($resultmax);
        $newSeq = $newSeq['maxSeq'] + 1;
    //**  Inserting a New Item  ***************************************//
    if ($_POST["submit"] == "Add") {

        $itemName = $_POST["itemName"];
        $itemPrice = $_POST["itemPrice"];
        $kgPrice = $_POST["kgPrice"];
        $itemSeq = $_POST["itemSeq"];
        $callJs = $_POST["callJs"];
        $visibled = $_POST["visibled"];
        $backColor = "#" . $_POST["backColor"];
        $itemPhoto = "";

        $cmd = "INSERT INTO item (itemName,itemPrice, kgPrice, itemPhoto, callJs, backColor, visibled) 
         VALUES ('" . $itemName . "'," . $itemPrice . "," . $kgPrice . ",'" . $itemPhoto . "','" . $callJs . "','" . $backColor . "', " . $visibled . ")";
        $result = mysql_query($cmd) or die("Item Insert Error: " . mysql_error());
        $itemId = mysql_insert_id();

        //**  Upload Photo  ***************************************//
        if (isset($_FILES["photo"]["name"])) {
            if (($_FILES["photo"]["type"] == "image/gif") || ($_FILES["photo"]["type"] == "image/jpg") || ($_FILES["photo"]["type"] == "image/jpeg") || ($_FILES["photo"]["type"] == "image/bmp") || ($_FILES["photo"]["type"] == "image/png")) {
                $photoPathinfo = pathinfo($_FILES["photo"]["name"]);
                $extension = $photoPathinfo['extension'];
                $imageName = "item_" . $itemId . "." . $extension;
                $itemfile = './images/' . $imageName;
                if ($extension == "jpg" || $extension == "png" || $extension == "bmp" || $extension == "jpeg" || $extension == "gif") {
                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $itemfile)) {
                        $cmd = "UPDATE item SET itemPhoto='" . $itemfile . "' WHERE itemId=" . $itemId;
                        $result = mysql_query($cmd) or die("Photo Update Error: " . mysql_error());
                    }
                }
            }
        }

        //**  Update Sequence  ***************************************//        
        
        if ($itemSeq == "" || $itemSeq > $newSeq) {
            $itemSeq = $newSeq;
        } else if ($itemSeq <= $newSeq) {

            $cmd = "SELECT * FROM item WHERE itemSeq>=" . $itemSeq . " ORDER BY itemSeq ASC";
            $result = mysql_query($cmd) or die("Bigger Item Seq Selection  Error: " . mysql_error());

            $newSeq = $itemSeq;
            while ($row = mysql_fetch_array($result)) {
                $newSeq++;
                $cmd = "UPDATE item SET itemSeq=" . $newSeq . " WHERE itemId=" . $row['itemId'];
                $result1 = mysql_query($cmd) or die("Bigger Item Seq Selection  Error: " . mysql_error());
            }
        }
        
        $cmd = "UPDATE item SET itemSeq='" . $itemSeq . "' WHERE itemId=" . $itemId;
        $result = mysql_query($cmd) or die("Photo Update Error: " . mysql_error());
    }
    //**  Update Item  ***************************************//
    else if ($_POST["submit"] == "Edit") {
        $itemId = $_POST["itemId"];
        $editPhoto = $_POST["editPhoto"];
        $itemName = $_POST["itemName"];
        $itemPrice = $_POST["itemPrice"];
        $kgPrice = $_POST["kgPrice"];
        $itemSeq = $_POST["itemSeq"];
        $callJs = $_POST["callJs"];
        $visibled = $_POST["visibled"];
        $backColor = "#" . $_POST["backColor"];
        $itemPhoto = "";
        $cmd = "UPDATE item SET 
              itemName='" . $itemName . "', 
              itemPrice='" . $itemPrice . "', 
              kgPrice='" . $kgPrice . "', 
              callJs='" . $callJs . "', 
              backColor='" . $backColor . "', 
              visibled='" . $visibled . "'  
              WHERE itemId=" . $itemId;
        $result = mysql_query($cmd) or die("Item Update Error: " . mysql_error());

        if (isset($_FILES["photo"]["name"])) {
            if (($_FILES["photo"]["type"] == "image/gif") || ($_FILES["photo"]["type"] == "image/jpg") || ($_FILES["photo"]["type"] == "image/jpeg") || ($_FILES["photo"]["type"] == "image/bmp") || ($_FILES["photo"]["type"] == "image/png")) {
                $photoPathinfo = pathinfo($_FILES["photo"]["name"]);
                $extension = $photoPathinfo['extension'];
                $imageName = "item_" . $itemId . "." . $extension;
                $itemfile = './images/' . $imageName;
                if ($extension == "jpg" || $extension == "png" || $extension == "bmp" || $extension == "jpeg" || $extension == "gif") {

                    if (file_exists($editPhoto))
                        unlink($editPhoto);

                    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $itemfile)) {
                        $cmd = "UPDATE item SET itemPhoto='" . $itemfile . "' WHERE itemId=" . $itemId;
                        $result = mysql_query($cmd) or die("Zero Seq Update Error: " . mysql_error());
                    }
                }
            }
        }
        //**  Update Sequence  ***************************************//


        $setItemSeq = $itemSeq;
        $cmd = "SELECT * FROM item WHERE itemId=" . $itemId;
        $result = mysql_query($cmd) or die("Item ID Selection  Error: " . mysql_error());
        $currentSeq = mysql_fetch_array($result);
        if ($itemSeq == "" || $itemSeq >= $newSeq) {//If new Item Seq Left Blank
            $cmd = "SELECT Max(itemSeq) AS maxSeq FROM item";
            $resultMaxSeq = mysql_query($cmd) or die("Max(itemSeq)  Error: " . mysql_error());
            $setItemSeq = mysql_fetch_array($resultMaxSeq);
            $setItemSeq = $setItemSeq['maxSeq'];

            $cmd = "SELECT * FROM item WHERE itemSeq>" . $currentSeq['itemSeq'] . " ORDER BY itemSeq ASC";
            $resultHigherSeq = mysql_query($cmd) or die("Bigger Item Seq Selection  Error: " . mysql_error());
            while ($rowHigherSeq = mysql_fetch_array($resultHigherSeq)) {
                $newSeq = $rowHigherSeq['itemSeq'] - 1;
                $cmd = "UPDATE item SET itemSeq=" . $newSeq . " WHERE itemId=" . $rowHigherSeq['itemId'];
                $updatHighereSeq = mysql_query($cmd) or die("Update HigherSeq Error: " . mysql_error());
            }
        } else if ($itemSeq > $currentSeq['itemSeq']) { //If new Item Seq Bigger than current seq num
            $cmd = "SELECT * FROM item WHERE itemSeq>" . $currentSeq['itemSeq'] . " AND itemSeq<=" . $itemSeq . " ORDER BY itemSeq ASC";
            $resultHigherSeq = mysql_query($cmd) or die("Bigger Item Seq Selection  Error: " . mysql_error());
            while ($rowHigherSeq = mysql_fetch_array($resultHigherSeq)) {
                $newSeq = $rowHigherSeq['itemSeq'] - 1;
                $cmd = "UPDATE item SET itemSeq=" . $newSeq . " WHERE itemId=" . $rowHigherSeq['itemId'];
                $updatHighereSeq = mysql_query($cmd) or die("Update HigherSeq Error: " . mysql_error());
            }
        } else if ($itemSeq < $currentSeq['itemSeq']) { //If new Item Seq Smaller than current seq num
            $cmd = "SELECT * FROM item WHERE itemSeq<" . $currentSeq['itemSeq'] . " AND itemSeq>=" . $itemSeq . " ORDER BY itemSeq DESC";
            $resultHigherSeq = mysql_query($cmd) or die("Bigger Item Seq Selection  Error: " . mysql_error());
            while ($rowHigherSeq = mysql_fetch_array($resultHigherSeq)) {
                $newSeq = $rowHigherSeq['itemSeq'] + 1;
                $cmd = "UPDATE item SET itemSeq=" . $newSeq . " WHERE itemId=" . $rowHigherSeq['itemId'];
                $updatHighereSeq = mysql_query($cmd) or die("Update HigherSeq Error: " . mysql_error());
            }
        }
        $cmd = "UPDATE item SET itemSeq='" . $setItemSeq . "' WHERE itemId=" . $itemId;
        $result = mysql_query($cmd) or die("Photo Update Error: " . mysql_error());
    }//closed isset($_POST["submit"]=="Edit")
} //closed isset($_POST["submit"])
//** Fetching Item Record for Editing Text Boxes ***************************************//
else if (isset($_GET["do"]) && $_GET["do"] == "edit") {
    $delId = $_GET["itemid"];
    $cmd = "SELECT * FROM item WHERE itemId=" . $delId;
    $result = mysql_query($cmd) or die("Selection Error: " . mysql_error());
    $row = mysql_fetch_array($result);
    $EdititemsArray['itemId'] = $row['itemId'];
    $EdititemsArray['itemName'] = $row['itemName'];
    $EdititemsArray['itemPrice'] = $row['itemPrice'];
    $EdititemsArray['kgPrice'] = $row['kgPrice'];
    $EdititemsArray['itemSeq'] = $row['itemSeq'];
    $EdititemsArray['itemPhoto'] = $row['itemPhoto'];
    $EdititemsArray['visibled'] = $row['visibled'];
    $EdititemsArray['callJs'] = $row['callJs'];
    $EdititemsArray['backColor'] = $row['backColor'];
    $EdititemsArray['submitvalue'] = "Edit";
} // Closed $_GET["do"] == "edit")
//** Generating Jquery DataTable  ***************************************//
$cmd = "SELECT * FROM item ORDER BY itemSeq ASC";
$result = mysql_query($cmd) or die("Selection Error: " . mysql_error());
while ($row = mysql_fetch_array($result)) {
    $itemsArray[$i]['itemId'] = $row['itemId'];
    $itemsArray[$i]['itemName'] = $row['itemName'];
    $itemsArray[$i]['itemPrice'] = $row['itemPrice'];
    $itemsArray[$i]['kgPrice'] = $row['kgPrice'];
    $itemsArray[$i]['itemSeq'] = $row['itemSeq'];
    $itemsArray[$i]['itemPhoto'] = $row['itemPhoto'];
    $itemsArray[$i]['backColor'] = $row['backColor'];
    //echo $row['backColor'];
    if ($row['callJs'] == 'setIceCreamItemId') {
        $itemsArray[$i]['callJs'] = "Unit and 1 Kg";
    } else {
        $itemsArray[$i]['callJs'] = "Only Unit";
    }

    if ($row['visibled'] == '1')
        $itemsArray[$i]['visibled'] = "Visible";
    else
        $itemsArray[$i]['visibled'] = "Hidden";
    $i++;   
    
    if ($row['itemSeq'] == '0') {
        $cmd = "SELECT Max(itemSeq) AS maxSeq FROM item";
        $result2 = mysql_query($cmd) or die("Max(itemSeq)  Error: " . mysql_error());
        $maxnewSeq = mysql_fetch_array($result2);
        $maxnewSeq = $maxnewSeq['maxSeq'] + 1;

        $cmd = "UPDATE item SET itemSeq='" . $maxnewSeq . "' WHERE itemId=" . $row['itemId'];
        $result3 = mysql_query($cmd) or die("Zero Seq Update Error: " . mysql_error());
    }
}


include("./bottom.php");
$smarty->assign('EdititemsArray', $EdititemsArray);
$smarty->assign('itemsArray', $itemsArray);
$smarty->display("items.tpl");
