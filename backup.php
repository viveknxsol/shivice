<?php
$backupDir = './backup/'; // full path, must be 777 and passwd-protected

include("include/config.inc.php");
set_time_limit(0);
$filePrefix = "om";
$time_now=mktime(date('h')+5,date('i')+30,date('s'));

$backup_file = $filePrefix.'_'.date('Y_m_d_h_i_s_A',$time_now).'.sql';

echo " Backup file Is Successfully Save ...!!   <a href='./backup/".$backup_file."'> Download </a> || <a href='index.php'> Home </a>";

$fp = fopen($backupDir . $backup_file, 'w');
$schema = '# Database Backup' .
          '#' . "\n" .
          '# Backup Date: ' . date('Y-m-d H:i:s') . "\n\n";
fputs($fp, $schema);

$tables_query = mysql_query('show tables');
while ($tables = mysql_fetch_array($tables_query)) 
{
  list(,$table) = each($tables);

  $schema = 'drop table if exists ' . $table . ';' . "\n" .
            'create table ' . $table . ' (' . "\n";

  $table_list = array();
  $fields_query = mysql_query("show fields from " . $table);
  while ($fields = mysql_fetch_array($fields_query))
  {
    $table_list[] = $fields['Field'];

    $schema .= '  ' . $fields['Field'] . ' ' . $fields['Type'];

    if (strlen($fields['Default']) > 0) $schema .= ' default \'' . $fields['Default'] . '\'';

    if ($fields['Null'] != 'YES') $schema .= ' not null';

    if (isset($fields['Extra'])) $schema .= ' ' . $fields['Extra'];

    $schema .= ',' . "\n";
  }
  $schema = preg_replace(",\n$',", '', $schema);

// add the keys
  $index = array();
  $keys_query = mysql_query("show keys from " . $table);
  while ($keys = mysql_fetch_array($keys_query)) 
  {
    $kname = $keys['Key_name'];

    if (!isset($index[$kname]))
    {
      $index[$kname] = array('unique' => !$keys['Non_unique'],
                             'fulltext' => ($keys['Index_type'] == 'FULLTEXT' ? '1' : '0'),
                             'columns' => array());
    }

    $index[$kname]['columns'][] = $keys['Column_name'];
  }

  while (list($kname, $info) = each($index)) 
  {
    $schema .= ',' . "\n";

    $columns = implode($info['columns'], ', ');

    if ($kname == 'PRIMARY')
      $schema .= '  PRIMARY KEY (' . $columns . ')';
    elseif ( $info['fulltext'] == '1' )
      $schema .= '  FULLTEXT ' . $kname . ' (' . $columns . ')';
    elseif ($info['unique'])
    {
      $schema .= '  UNIQUE ' . $kname . ' (' . $columns . ')';
      $schema .= '  KEY ' . $kname . ' (' . $columns . ')';
    }
  }

  $schema .= "\n" . ');' . "\n\n";
  fputs($fp, $schema);
// dump the data
  $rows_query = mysql_query("select " . implode(',', $table_list) . " from " . $table);
  while ($rows = mysql_fetch_array($rows_query))
  {
    $schema = 'insert into ' . $table . ' (' . implode(', ', $table_list) . ') values (';

    reset($table_list);
    while (list(,$i) = each($table_list)) 
    {
      if (!isset($rows[$i]))
        $schema .= 'NULL, ';
      elseif ( trim($rows[$i]) != '' )
      {
        $row = addslashes($rows[$i]);
        $row = preg_replace("'\n#'", '\n'.'\#', $row);
        $schema .= '\'' . $row . '\', ';
      }
      else
        $schema .= '\'\', ';
    }
    $schema = preg_replace("', $'", '', $schema) . ');' . "\n";
    fputs($fp, $schema);
  }
}
fclose($fp);
?>