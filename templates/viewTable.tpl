<html>
    <body style="background-color:#F5F5DC;">
        <table align="left" border="2" id="tableNo" class="waiterdiv" cellpadding="10" cellspacing="0" width="480px" style="background-color:#F5F5DC;" >
            <th colspan="7"><b><font size="5px" color="blue"> Table : {$orderArray[0].tableId}&nbsp&nbsp{if {$orderArray[0].tablePart} == 'B'} B{elseif {$orderArray[0].tablePart} == 'C' } C {/if}&nbsp &nbsp &nbsp Waiter : {$orderArray[0].waiterName}</font></b></th>            
            {if $recordFound == 1}
                <tr>                    
                    <td class="table1"><b><font size="5px" style="color:grin;">P</font></b></td>
                    <td class="table1"><b><font size="5px">Item</font></b></td>    
                    <td class="table1"><b><font size="5px">Qty</font></b></td>
                    <td class="table1"><b><font size="5px">Amt</font></b></td>
                    <td class="table1"><b><font size="5px">Rate</font></b></td>
                    <td class="table1"><b><font size="5px">Time</font></b></td>
                    <td class="table1"><b><font size="5px">User</font></b></td>
                </tr>
                {section name=iSec loop=$orderArray}
                    <tr>
                        <td class="table2" align="center"><div id="takeawaydiv{$smarty.section.iSec.index}">{$orderArray[iSec].parcel}</div></td>
                        <td class="table2" align="left" NOWRAP><font size="5px">{$orderArray[iSec].itemName}&nbsp;{if $orderArray[iSec].weight > 0}{if $orderArray[iSec].weight == 1000}{$orderArray[iSec].quantity}kg{else}{$orderArray[iSec].weight}gm{/if}{else}{/if}</font></td>
                        <td align="right"><font size="5px">{$orderArray[iSec].quantity}</font></td>
                        <td align="right"><font size="5px"><b>{$orderArray[iSec].amount}</b></font></td>
                        <td class="table2" align="right"><font size="5px">{$orderArray[iSec].itemPrice}</font></td>                                             
                        <td class="table2" align="left"><font size="5px">{$orderArray[iSec].orderTime|date_format:"%H:%M"}</font></td>
                        <td class="table2" align="left"><font size="5px">{$orderArray[iSec].userName}</font></td>
                    </tr>
                    <script>
                        count++;
                    </script>
                {/section}
                    <tr>
                        <td>&nbsp;</td>
                        <td class="table1"><b><font size="5px">Total</font></b></td>
                        <td align="right"><font size="5px">{$totalItems}</font></td>
                        <td align="right"><font size="5px"><b>{$totalAmount}</b></font></td>
                        <td colspan="3">&nbsp;</td>
                    </tr>

            {else}
                <tr>
                    <th colspan="7">Record Not Found For Current Table</th>
                </tr>
            {/if}
        </table>
    </body>
</html>