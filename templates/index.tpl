{block name=head}
    <title>Om Shiv Ice Cream</title>
    <link type="text/css" rel="stylesheet" href="css/window8Style.css">
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
{/block}
{block name=body}
    <!-- =================== Main Div ============================== -->
    <div class="mainDiv">
        <!-- =================== Sidebar Div ========================== -->
        <div class="sideBarDiv">
            <div class="logo">
                <img src="./logo/serverClient.png" hspace="93" width="200px" height="62px">
            </div>
            <div id="car" class="car" >
                <img src="./images/car.jpg" height="50px" width="100px" onclick="changeUrlCar({$waiterVar},{$tableVar}, 'C');">
            </div>
            <div 
                {if {$smarty.get.tablePart} != 'C'}style="display:none;"{/if} style="background-color:#DE99FE; box-shadow: 5px 5px 3px #888888; height:3em; width: 12em; -moz-border-radius: 1em 1em 1em 1em; 
                border-radius: 1em 1em 1em 1em;" align="center" class="carInnerDiv" id="C" onclick="changeUrlCar({$waiterVar},{$tableVar}, this.id);">
                <font size="6px"><b>Add New Car</b></font>
            </div>
            <div
                {if {$smarty.get.tablePart} == 'C'}style="display:none;"{/if} style="background-color:white; height:3em; width: 12em;" align="center" class="carInnerDiv" id="C" >
            </div>
            <div id="home" class="home">
                <img width="90px" src="./images/home.jpg" height="50px">
            </div>
            <!-- =================== Car Div ========================== -->
            <div class="carDiv" {if {$smarty.get.tablePart} != 'C'}style="display:none;"{/if} id="carDiv">
                <iframe src="carTables.php" width="387" height="495" scrolling="yes" id="carIfrem"></iframe>
            </div>
            <div {if {$smarty.get.tablePart} != 'C'} style="display:none;"{/if}>
                <table id="waiter" border="1" cellpadding="10" width="390px" height="245px">

                    <tr>
                        <td {if {$smarty.get.waiter} == 1}style="background-color:red;"{/if}class="tableNo" align="center" id="1" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">T</a></td>
                        <td {if {$smarty.get.waiter} == 2}style="background-color:red;"{/if}class="tableNo" align="center" id="2" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">N</a></td>
                        <td {if {$smarty.get.waiter} == 3}style="background-color:red;"{/if}class="tableNo" align="center" id="3" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">G</a></td>
                        <td {if {$smarty.get.waiter} == 4}style="background-color:red;"{/if}class="tableNo" align="center" id="4" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">A</a></td>
                    </tr>
                    <tr>
                        <td {if {$smarty.get.waiter} == 5}style="background-color:red;"{/if}class="tableNo" align="center" id="5" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">J</a></td>
                        <td {if {$smarty.get.waiter} == 6}style="background-color:red;"{/if}class="tableNo" align="center" id="6" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">D</a></td>
                        <td {if {$smarty.get.waiter} == 7}style="background-color:red;"{/if}class="tableNo" align="center" id="7" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">V</a></td>
                        <td {if {$smarty.get.waiter} == 8}style="background-color:red;"{/if}class="tableNo" align="center" id="8" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">B</a></td>
                    </tr>
                    <tr>
                        <td {if {$smarty.get.waiter} == 9}style="background-color:red;"{/if}class="tableNo" align="center" id="9" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">F</td>
                        <td {if {$smarty.get.waiter} == 10}style="background-color:red;"{/if}class="tableNo" align="center" id="10" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">O</td>
                        <td {if {$smarty.get.waiter} == 11}style="background-color:red;"{/if}class="tableNo" align="center" id="11" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">M</td>
                        <td {if {$smarty.get.waiter} == 12}style="background-color:red;"{/if}class="tableNo" align="center" id="12" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">L</td>
                    </tr>
                    <tr>
                        <td {if {$smarty.get.waiter} == 13}style="background-color:red;"{/if}class="tableNo" align="center" id="13" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">U</td>
                        <td {if {$smarty.get.waiter} == 14}style="background-color:red;"{/if}class="tableNo" align="center" id="14" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">H</td>
                        <td {if {$smarty.get.waiter} == 15}style="background-color:red;"{/if}class="tableNo" align="center" id="15" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">K</td>
                        <td {if {$smarty.get.waiter} == 16}style="background-color:red;"{/if}class="tableNo" align="center" id="16" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">P</td>
                    </tr>                    
                </table>
            </div>

            <div class="currentTableDiv" id="tableWaiter" {if {$smarty.get.tablePart} == 'C'}style="display:none;"{/if}>
                <table class="selectTable" >
                    <tr>
                        <td valign="top">
                            <table id="waiter" border="1" cellpadding="10" width="390px" height="290px">
                                <tr>
                                    <th colspan="4" style="font-size:30px; color:blue; ">Waiter</th>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.waiter} == 1}style="background-color:red;"{/if}class="tableNo" align="center" id="1" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">T</a></td>
                                    <td {if {$smarty.get.waiter} == 2}style="background-color:red;"{/if}class="tableNo" align="center" id="2" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">N</a></td>
                                    <td {if {$smarty.get.waiter} == 3}style="background-color:red;"{/if}class="tableNo" align="center" id="3" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">G</a></td>
                                    <td {if {$smarty.get.waiter} == 4}style="background-color:red;"{/if}class="tableNo" align="center" id="4" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">A</a></td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.waiter} == 5}style="background-color:red;"{/if}class="tableNo" align="center" id="5" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">J</a></td>
                                    <td {if {$smarty.get.waiter} == 6}style="background-color:red;"{/if}class="tableNo" align="center" id="6" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">D</a></td>
                                    <td {if {$smarty.get.waiter} == 7}style="background-color:red;"{/if}class="tableNo" align="center" id="7" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">V</a></td>
                                    <td {if {$smarty.get.waiter} == 8}style="background-color:red;"{/if}class="tableNo" align="center" id="8" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">B</a></td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.waiter} == 9}style="background-color:red;"{/if}class="tableNo" align="center" id="9" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">F</td>
                                    <td {if {$smarty.get.waiter} == 10}style="background-color:red;"{/if}class="tableNo" align="center" id="10" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">O</td>
                                    <td {if {$smarty.get.waiter} == 11}style="background-color:red;"{/if}class="tableNo" align="center" id="11" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">M</td>
                                    <td {if {$smarty.get.waiter} == 12}style="background-color:red;"{/if}class="tableNo" align="center" id="12" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">L</td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.waiter} == 13}style="background-color:red;"{/if}class="tableNo" align="center" id="13" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">U</td>
                                    <td {if {$smarty.get.waiter} == 14}style="background-color:red;"{/if}class="tableNo" align="center" id="14" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">H</td>
                                    <td {if {$smarty.get.waiter} == 15}style="background-color:red;"{/if}class="tableNo" align="center" id="15" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">K</td>
                                    <td {if {$smarty.get.waiter} == 16}style="background-color:red;"{/if}class="tableNo" align="center" id="16" onclick="changeUrlWaiter(this.id,{$tableVar}, '{$tablePartVar}');">P</td>
                                </tr>                                                    
                            </table>	
                            <table id="tableNo" clsss="waiterdiv" cellpadding="8" border="1" width="390px" height="450px">
                                <tr>
                                    <th colspan="5" style="font-size:30px; color:blue;"> Table No : &nbsp&nbsp&nbsp&nbsp
                                        <a href="index.php?waiter={$waiterVar}&table={$tableVar}&tablePart=A">{if $tablePartVar=='A'}{else}<font color="green">B</font>{/if}</a>&nbsp;
                                        <a href="index.php?waiter={$waiterVar}&table={$tableVar}&tablePart=B">{if $tablePartVar=='B'}{else}B{/if}</a>
                                    </th>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.table} == 1}style="background-color:red;"{/if}align="center" class="tableNo" id="1" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 1</td>
                                    <td {if {$smarty.get.table} == 2}style="background-color:red;"{/if}align="center" class="tableNo" id="2" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 2</td>
                                    <td {if {$smarty.get.table} == 3}style="background-color:red;"{/if}align="center" class="tableNo" id="3" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 3</td>
                                    <td {if {$smarty.get.table} == 4}style="background-color:red;"{/if}align="center" class="tableNo" id="4" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 4</td>
                                    <td {if {$smarty.get.table} == 5}style="background-color:red;"{/if}align="center" class="tableNo" id="5" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 5</td>
                                </tr>                                                                                                
                                <tr>                                                                                                 
                                    <td {if {$smarty.get.table} == 6}style="background-color:red;"{/if}class="tableNo" align="center" id="6" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 6</td>
                                    <td {if {$smarty.get.table} == 7}style="background-color:red;"{/if}class="tableNo" align="center" id="7" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 7</td>
                                    <td {if {$smarty.get.table} == 8}style="background-color:red;"{/if}class="tableNo" align="center" id="8" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 8</td>
                                    <td {if {$smarty.get.table} == 9}style="background-color:red;"{/if}class="tableNo" align="center" id="9" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');"> 9</td>
                                    <td {if {$smarty.get.table} == 10}style="background-color:red;"{/if}class="tableNo" align="center" id="10" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">10</td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.table} == 11}style="background-color:red;"{/if}class="tableNo" align="center" id="11" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">11</td>
                                    <td {if {$smarty.get.table} == 12}style="background-color:red;"{/if}class="tableNo" align="center" id="12" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">12</td>
                                    <td {if {$smarty.get.table} == 13}style="background-color:red;"{/if}class="tableNo" align="center" id="13" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">13</td>
                                    <td {if {$smarty.get.table} == 14}style="background-color:red;"{/if}class="tableNo" align="center" id="14" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">14</td>
                                    <td {if {$smarty.get.table} == 15}style="background-color:red;"{/if}class="tableNo" align="center" id="15" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">15</td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.table} == 16}style="background-color:red;"{/if}class="tableNo" align="center" id="16" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">16</td>
                                    <td {if {$smarty.get.table} == 17}style="background-color:red;"{/if}class="tableNo" align="center" id="17" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">17</td>
                                    <td {if {$smarty.get.table} == 21}style="background-color:red;"{/if}class="tableNo1" align="center" id="21" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">21</td>
                                    <td {if {$smarty.get.table} == 22}style="background-color:red;"{/if}class="tableNo1" align="center" id="22" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">22</td>
                                    <td {if {$smarty.get.table} == 23}style="background-color:red;"{/if}class="tableNo1" align="center" id="23" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">23</td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.table} == 24}style="background-color:red;"{/if}class="tableNo1" align="center" id="24" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">24</td>
                                    <td {if {$smarty.get.table} == 25}style="background-color:red;"{/if}class="tableNo1" align="center" id="25" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">25</td>
                                    <td {if {$smarty.get.table} == 26}style="background-color:red;"{/if}class="tableNo1" align="center" id="26" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">26</td>
                                    <td {if {$smarty.get.table} == 27}style="background-color:red;"{/if}class="tableNo1" align="center" id="27" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">27</td>
                                    <td {if {$smarty.get.table} == 31}style="background-color:red;"{/if}class="tableNo2" align="center" id="31" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">31</td>
                                </tr>
                                <tr>
                                    <td {if {$smarty.get.table} == 32}style="background-color:red;"{/if}class="tableNo2" align="center" id="32" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">32</td>
                                    <td {if {$smarty.get.table} == 33}style="background-color:red;"{/if}class="tableNo2" align="center" id="33" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">33</td>
                                    <td {if {$smarty.get.table} == 34}style="background-color:red;"{/if}class="tableNo2" align="center" id="34" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">34</td>
                                    <td {if {$smarty.get.table} == 35}style="background-color:red;"{/if}class="tableNo2" align="center" id="35" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">35</td>
                                    <td {if {$smarty.get.table} == 36}style="background-color:red;"{/if}class="tableNo2" align="center" id="36" onclick="changeUrlTable({$waiterVar}, this.id, '{$tablePartVar}');">36</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>   
            </div>     
            <!-- ========================== In Kilogram Div  ========================== -->
            <div class="inkgDiv">
                <div class="gm2" id="250" onclick="setWeight(this.id)">
                    250
                </div>	
                <div class="gm2" id="500" onclick="setWeight(this.id)">
                    500
                </div>
                <div class="gm2" id="750" onclick="setWeight(this.id)">
                    750
                </div>
                <div class="gm2" id="1000" onclick="setWeight(this.id)">
                    1kg
                </div>
            </div>
        </div>   


        <!-- ======================================================= Contain Div ===================================================================================== -->
        <div class="containDiv">
            {section name="itemsec" loop=$itemsArray}
                <div style="background-color: {$itemsArray[itemsec].backColor}" class="itemsDiv items" id="{$itemsArray[itemsec].itemId}" onclick="{$itemsArray[itemsec].callJs}(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');"   >
                    <table width="127px" align="center" border="0">
                        <tr>
                            <th align="center">
                                {$itemsArray[itemsec].itemName}
                            </th>
                        </tr>                        

                        <tr>
                            <td align="center">
                                <img class="items" src="{$itemsArray[itemsec].itemPhoto}" width="100" height="100">
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="price" valign="top">
                                <img src="images/rupee.png" width="18px" height="18px">{$itemsArray[itemsec].itemPrice}
                            </td>
                        </tr>
                    </table>
                </div>
            {/section}

            <div id="box2" style="display:none;">
                <div id="box2">
                    <div id="calculator" class="drag">
                        <div class="calculatorcontainer">
                            <div id="version"><a href=#" title="calculator" onclick="return false">Calculator</a></div>
                            <div id="control"><img src="images/deletesmall.png" id="close" height="20px" width="20px" style="position:relative;top:-1px" /></div>
                            <input type="button" value="1" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="2" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="3" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');"><br />
                            <input type="button" value="4" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="5" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="6" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');"><br />
                            <input type="button" value="7" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="8" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="9" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');"><br />
                            <input type="button" value="-" id="minus" onclick="setQuntityPlusMinus(this)" class="operation">
                            <input type="button" value="10" onclick="addQuantity(this,{$smarty.get.waiter},{$smarty.get.table}, '{$smarty.get.tablePart}');">
                            <input type="button" value="+" id="plus" onclick="setQuntityPlusMinus(this)" class="operation">
                        </div>
                    </div>
                </div>  
            </div>
        </div>

        <!-- ========================== Right Side Bar Div ========================== -->
        <div class="rightableMain">
            <div class="rightable">
                <table align="center" cellpadding="6">
                    <tr>
                        <td align="right" class="links">
                            {if $s_userType == "admin"}
                                <a href="index.php" class="links"><font color="red" size="5px">Home</font></a> <font color="black"> | </font>
                                <a href="items.php" class="links"><font color="red" size="5px"> Items </font> </a> <font color="black"> | </font>            
                                <a href="closeTables.php" class="links"> <font color="red" size="5px">Tables </font> </a> <font color="black"> | </font>
                                <a href="changePassword.php" class="links"> <font color="red" size="5px"> Change Password</font> </a> <font color="black"> | </font>
                                <a href="backup.php" class="links"><font color="red" size="5px">Back Up</font></a> <font color="black"> | </font> 
                                <a href="logout.php" class="links"><font color="red" size="5px"> LogOut</font></a>
                                {else}
                                <a href="index.php" class="links"><font size="5px"> Home</font> </a> |                                 
                                <a href="closeTables.php" class="links"><font size="5px"> Tables </font></a> | 
                                <!--<a href="items.php" class="links"> <font size="5px">Items </font> </a> |
                                <!--<a href="backup.php" class="links"><font size="5px"> Back Up </font></a> | -->
                                <a href="changePassword.php" class="links"><font size="5px"> Change Password </a> | 
                                <a href="logout.php" class="links"><font color="red" size="5px"> LogOut</font></a>
                                {/if} 
                        </td>
                    </tr>
                    <tr>
                        <td>  
                            <iframe id="selectedTableIframe" src="selectedTable.php?waiterId={$waiterVar}&tableId={$tableVar}&tablePart={$tablePartVar}" width="500" height="350" scrolling="yes"></iframe>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <iframe id="currentTableIfrem" src="currentTables.php?waiter={$waiterVar}&table={$tableVar}&orderId={$orderId}" width="500" height="615" scrolling="yes" ></iframe>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
{/block}