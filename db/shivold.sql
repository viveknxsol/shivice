-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 04, 2013 at 05:29 AM
-- Server version: 5.0.67-community-nt
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shiv`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` int(11) NOT NULL auto_increment,
  `itemPrice` int(11) NOT NULL,
  `itemName` varchar(100) character set utf8 NOT NULL,
  PRIMARY KEY  (`itemId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `itemPrice`, `itemName`) VALUES
(1, 35, 'Ice Cream'),
(2, 40, 'Ice Cream'),
(3, 60, 'Shiv Special'),
(4, 30, 'King Cone'),
(5, 25, 'Candy'),
(6, 35, 'Sp. Mava Candy'),
(7, 100, 'Sunday'),
(8, 50, 'Juice'),
(9, 70, 'Faluda'),
(10, 45, 'Chickoo Pl'),
(11, 75, 'Vanila/Coffee Pl'),
(12, 95, 'Shake Pl'),
(13, 140, 'Shake Sp'),
(14, 120, 'Noodles'),
(15, 140, 'Mush/Manch Noodles'),
(16, 120, 'Rice/Bhel'),
(17, 140, 'Pan/Mus Rice'),
(18, 120, 'M.Gravy/Dry'),
(19, 180, 'Pan.Ch.Gravy/Dry'),
(20, 190, 'Trippal Schezuan'),
(21, 150, 'Pizza'),
(22, 180, 'Shiv Special Pizaa'),
(23, 130, 'Club'),
(24, 90, 'Grill'),
(25, 85, 'Non Grill'),
(26, 130, 'Oven Tost'),
(27, 80, 'Garlic Bread'),
(29, 25, 'Cold Drink'),
(30, 20, 'Mineral Water'),
(31, 30, 'Extra Dry Fruits'),
(32, 40, 'Mashroom Paneer'),
(33, 20, 'Manchurian Chopsuey'),
(34, 30, 'Extra Per Toppings'),
(35, 10, 'Extra Chutney'),
(36, 50, 'Chickoo Spacial'),
(37, 80, 'Vanila/Coffee Spacial'),
(38, 100, 'Shake Spacial'),
(39, 150, 'Shake Sp. Sp.'),
(40, 85, 'Burger'),
(41, 80, 'French Fries'),
(42, 18, 'Ice Cream Half'),
(43, 20, 'Ice Cream Half'),
(44, 30, 'Ice Cream Half'),
(45, 350, 'Ice Cream'),
(46, 400, 'Ice Cream'),
(47, 600, 'Shiv Special IceCream');

-- --------------------------------------------------------

--
-- Table structure for table `ordereditems`
--

CREATE TABLE IF NOT EXISTS `ordereditems` (
  `orderedItemsId` int(11) NOT NULL auto_increment,
  `orderId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `quantity` double NOT NULL default '0',
  `weight` int(11) NOT NULL default '0',
  `orderTime` time default NULL,
  `userName` varchar(255) character set utf8 default NULL,
  PRIMARY KEY  (`orderedItemsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ordermaster`
--

CREATE TABLE IF NOT EXISTS `ordermaster` (
  `orderId` int(11) NOT NULL auto_increment,
  `waiterId` int(11) NOT NULL,
  `tableId` int(11) NOT NULL,
  `tablePart` varchar(5) character set utf8 NOT NULL default 'A',
  `startTime` time NOT NULL,
  `startDate` date default NULL,
  `orderstatus` varchar(100) character set utf8 NOT NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL auto_increment,
  `userName` varchar(500) character set utf8 default NULL,
  `password` varchar(50) character set utf8 default NULL,
  `userType` varchar(255) NOT NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `userName`, `password`, `userType`) VALUES
(1, 'om', 'om', 'admin'),
(2, 'om1', 'om1', 'local');

-- --------------------------------------------------------

--
-- Table structure for table `waiter`
--

CREATE TABLE IF NOT EXISTS `waiter` (
  `waiterId` int(11) NOT NULL auto_increment,
  `waiterName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`waiterId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `waiter`
--

INSERT INTO `waiter` (`waiterId`, `waiterName`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D'),
(5, 'E'),
(6, 'F'),
(7, 'G'),
(8, 'H'),
(9, 'I'),
(10, 'J'),
(11, 'K'),
(12, 'L'),
(13, 'M'),
(14, 'N'),
(15, 'O'),
(16, 'P');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
