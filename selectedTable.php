<?php

include("include/config.inc.php");
$tableId  = isset($_REQUEST['tableId']) ? $_REQUEST['tableId']   : 0;
$waiterId = isset($_REQUEST['waiterId']) ? $_REQUEST['waiterId'] : 0;
$tablePart = isset($_REQUEST['tablePart']) ? $_REQUEST['tablePart'] : 0;
$waiterName = 0;
$totalAmount = 0;
$waiterNames = "";
$tableArray = array();
$print = isset($_REQUEST['print']) ? $_REQUEST['print'] : 0;
$orderArray = array();
$i = 0;
$recordFound = 0;
$l = 0;
$orderId  = 0;
$joinQueryResult = array();

if(isset($_REQUEST['tableId']) && $_REQUEST['tableId'] > 0)
{
  $selectOrderMaster    = "SELECT orderId,ordermaster.waiterId,ordermaster.tablePart,waiterName
                             FROM ordermaster
                             LEFT JOIN waiter 
                               ON waiter.waiterId = ordermaster.waiterId
                            WHERE ordermaster.orderstatus = 'L'
                              AND  ordermaster.tablePart='".$tablePart."'
                              AND ordermaster.tableId = ".$_REQUEST['tableId'];
  $selectOrderMasterRes = mysql_query($selectOrderMaster);
  while($selectOrderMasterRow = mysql_fetch_array($selectOrderMasterRes))
  {
    $tableArray['orderId']    = $selectOrderMasterRow['orderId'];
    $tableArray['waiterId']   = $selectOrderMasterRow['waiterId'];
    $tableArray['waiterName'] = $selectOrderMasterRow['waiterName'];
    $tableArray['tablePart']  = $selectOrderMasterRow['tablePart'];
  }
}
else if($orderId != 0)
{
  $selectOrderMaster    = "SELECT orderId,ordermaster.waiterId,waiterName
                             FROM ordermaster
                             LEFT JOIN waiter ON waiter.waiterId = ordermaster.waiterId
                            WHERE ordermaster.orderstatus = 'L'
                              AND ordermaster.orderId = ".$orderId."
                              AND  ordermaster.tablePart='".$tablePart."' " ;
  $selectOrderMasterRes = mysql_query($selectOrderMaster);
  if($selectOrderMasterRow = mysql_fetch_array($selectOrderMasterRes))
  {
  	$waiterId   = $selectOrderMasterRow['waiterId'];
  	$waiterName = $selectOrderMasterRow['waiterName'];
  }
}

if($waiterName == "")
{
  $selectWaiter    = "SELECT waiterId,waiterName 
	                      FROM waiter
	                     WHERE waiterId = ".$waiterId;
  $selectWaiterRes = mysql_query($selectWaiter);
  if($selectWaiterResRow = mysql_fetch_array($selectWaiterRes))
  {
    $waiterName = $selectWaiterResRow['waiterName'];
  }
}
if($waiterId != 0 && $tableId != 0)
{
  $selectOrder    = "SELECT item.itemName,item.itemPrice, item.kgPrice, ordereditems.quantity,ordereditems.orderTime,ordereditems.parcel,ordereditems.ordereditemsId ,ordereditems.orderId,waiter.waiterName,
                            ordermaster.tableId,ordereditems.itemId,waiter.waiterId,ordermaster.tableId,ordereditems.weight,ordereditems.userName
                       FROM ordereditems 
                  LEFT JOIN ordermaster ON ordereditems.orderId = ordermaster.orderId
                  LEFT JOIN item ON ordereditems.itemId =  item.itemId
                  LEFT JOIN waiter ON ordermaster.waiterId = waiter.waiterId
                      WHERE ordermaster.tableId = ".$tableId."
                        AND ordermaster.waiterId = ".$waiterId."
                        AND ordermaster.orderstatus = 'L' 
                        AND ordermaster.tablePart='".$tablePart."'
                   ORDER BY ordereditems.orderedItemsId DESC";
  $selectOrderRes = mysql_query($selectOrder);
  if(mysql_num_rows($selectOrderRes) > 0)
  {
    while($orderRow = mysql_fetch_array($selectOrderRes))
    {
      $recordFound = 1;
      $orderArray[$i]['weight']          = $orderRow['weight'];
      $orderArray[$i]['orderId']         = $orderRow['orderId'];
      $orderArray[$i]['waiterId']        = $orderRow['waiterId'];
      $orderArray[$i]['tableId']         = $orderRow['tableId'];
      $orderArray[$i]['itemName']        = $orderRow['itemName'];      
      $orderArray[$i]['kgPrice']         = $orderRow['kgPrice'];
      $orderArray[$i]['quantity']        = $orderRow['quantity'];
      $orderArray[$i]['userName']        = $orderRow['userName'];
      
      if($orderArray[$i]['weight'] == 0)
      {
       $orderArray[$i]['amount']         = $orderRow['itemPrice'] * $orderRow['quantity'];
       $orderArray[$i]['itemPrice']       = $orderRow['itemPrice'];
      }
      else
      {
        $orderArray[$i]['amount']        = ceil((($orderRow['kgPrice'] * $orderRow['weight'])/1000)*$orderRow['quantity']);
        $orderArray[$i]['itemPrice']       = $orderRow['kgPrice'];        
      }
      $waiterName                        = $orderRow['waiterName'];
      $orderArray[$i]['orderTime']       = $orderRow['orderTime'];    
      $orderArray[$i]['itemId']          = $orderRow['itemId']."\n";    
      $orderArray[$i]['ordereditemsId']  = $orderRow['ordereditemsId'];
      $orderArray[$i]['parcel']          = $orderRow['parcel'];
      
      $totalAmount                       = $totalAmount + $orderArray[$i]['amount'];
      $i++;
    }	
  }

}
else
{
	$recordFound = 0;
}	
if($waiterId != 0 && $tableId != 0)
{
  $joinQuery = " SELECT ordereditems.orderedItemsId, SUM( ordereditems.quantity * item.itemPrice ) AS totalamount, sum( quantity) AS totalItem
                   FROM ordermaster
                   JOIN ordereditems ON ordermaster.orderId = ordereditems.orderId
                   JOIN item ON item.itemId = ordereditems.itemId
                   JOIN waiter ON waiter.waiterId = ordermaster.waiterId
                  WHERE ordermaster.orderstatus = 'L'
                    AND  ordermaster.tableId='".$tableId."'
                    AND  ordermaster.tablePart='".$tablePart."'
                  GROUP BY ordermaster.tableId
                  ORDER BY ordereditems.orderedItemsId DESC";
                  
                  
   $joinQueryRes = mysql_query($joinQuery);
   $l=0;
   while($joinQueryRow = mysql_fetch_array($joinQueryRes))
   {
   	 $joinQueryResult[$l]['totalamount']    = $joinQueryRow['totalamount'];
   	 $joinQueryResult[$l]['totalItem']      = $joinQueryRow['totalItem'];
   	 $l++;
   }
}
include("./bottom.php");
$smarty->assign('recordFound',$recordFound);
$smarty->assign('orderArray',$orderArray);
$smarty->assign('tableArray',$tableArray);
$smarty->assign('tableId',$tableId);
$smarty->assign('tablePart',$tablePart);
$smarty->assign('waiterName',$waiterName);
$smarty->assign('waiterId',$waiterId);
$smarty->assign('totalAmount',$totalAmount);
$smarty->assign('joinQueryResult', $joinQueryResult);
$smarty->assign('waiterNames', $waiterNames);
$smarty->display('selectedTable.tpl');
?>