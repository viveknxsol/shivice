{block name=head}
<script src="js/jquery.min.js">
</script>
  <script type="text/javascript">
    function printFun(orderIdVar)
    {
     var url = "amountListPdf1.php?orderId="+orderIdVar;
     var win = window.open("amountListPdf1.php?orderId="+orderIdVar);
	 win.window.print();        
      setTimeout(function() { 
       win.close();	
       setTimeout("location.reload(true);", 500);
       }, 500);
    }
    
    //Page Refresh in 5 sec..
    //window.onload = setupRefresh;
    //function setupRefresh() {
     // setTimeout("refreshPage();", 5000);
    //}
    //function refreshPage() {
      //window.location = location.href;
    //}
	
  </script>
  <style>
    body
    {
      background-color:#F5F5DC;
    }
  </style>
{/block}
{block name=body}
<table align="left"  border="1"  cellpadding="5" cellspacing="0" id="tableNo" class="waiterdiv" style="background-color:#F5F5DC;">
  <tr>
    <td align="center" class="table1" colspan="9" valign="top"><b><font size="5px" color="blue">Order Master</font></b></td>
      <tr>
        <td class="table1" ><b><font size="5px">Print Bill</font></b></td>
        <td class="table1"><b><font size="5px">Table</font></b></td>
        <td class="table1"><b><font size="5px">Waiter</font></b></td>
        <td class="table1" align="right"><b><font size="5px">Items</font></b></td>
        <td class="table1" align="right"><b><font size="5px">Amount</font></b></td>
        <td class="table1"><b><font size="5px">Start</font></b></td>
        <td class="table1"><b><font size="5px">Last Order</font></b></td>
      </tr>
      {section name="sec" loop=$joinResult}
        <tr>
          <td class="table2" align="center" ondblclick="printFun({$joinResult[sec].orderId});"><font size="5px">Print</font></td>
          <td class="table2" align="center" {if $joinResult[sec].tablePart == 'C'}style="background-color:#38BEB7;"
                                            {elseif $joinResult[sec].tableId>=21 && $joinResult[sec].tableId<=27}style="background-color:green;"
                                            {elseif $joinResult[sec].tableId>=31 && $joinResult[sec].tableId<=36}style="background-color:yellow;"
                                                {/if}>{if $joinResult[sec].tablePart == 'C'}<font size="5px" color="red">
                                                    {else}<font size="8px" color="red">{/if}<b><a href="index.php?waiter={$joinResult[sec].waiterId}&table={$joinResult[sec].tableId}&tablePart={$joinResult[sec].tablePart}" target="_top" style="color:black; text-decoration: none; ">{$joinResult[sec].tableId} {if $joinResult[sec].tablePart == 'B'}/ B {elseif $joinResult[sec].tablePart == 'C'}/ C {/if}</a></b></font></td>
          <td class="table2" align="center"><font size="7px" color="blue">{$joinResult[sec].waiterName}</font></td>
          <td class="table2" align="right"><font size="5px"> {$joinResult[sec].totalItem}</font></td>
          <td class="table2" align="right"><font size="5px"> <b>{$joinResult[sec].totalAmount}</b></font></td>
          <td class="table2" align="center"><font size="5px">{$joinResult[sec].startTime|date_format:"%H:%M"}</font></td>
          <td class="table2" align="center"><font size="5px">{$joinResult[sec].lastordertime|date_format:"%H:%M"}</font></td>
        </tr>
      {/section}
  </tr>
</table>
{/block}